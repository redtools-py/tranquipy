import React from 'react'
import { Link } from 'react-scroll'

export default function NavbarHome({ isSticky }) {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <>
      <header className={`bg-teal-800 z-30 sm:flex sm:justify-between sm:items-center sm:px-4 sm:py-2 shadow-lg w-screen fixed top-0 left-0 navbar-ticky`}>
        <div className="flex items-center justify-between px-4 py-2 sm:p-0">
          <div>
            <img className="w-auto h-8 mx-auto" src="icono-blanco.svg" alt="Tranqui | Diseño e Informática"/>
            {/* <svg className="fill-current text-white w-auto h-6 mx-auto" viewBox="0 0 147.18 52.91"></svg> */}
          </div>
          <div className="sm:hidden">
            <button onClick={() => setIsOpen(!isOpen)} className="block text-gray-500 focus:text-white focus:outline-none hover:text-white" type="button">
              <svg className="h-6 w-6 fill-current" x="0px" y="0px" viewBox="0 0 512 512">
                {isOpen && <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717			L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859			c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287			l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285			L284.286,256.002z" />}
                {!isOpen && <><path d="M438,0H74C33.196,0,0,33.196,0,74s33.196,74,74,74h364c40.804,0,74-33.196,74-74S478.804,0,438,0z M438,108H74			c-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34S456.748,108,438,108z" />				<path d="M438,182H74c-40.804,0-74,33.196-74,74s33.196,74,74,74h364c40.804,0,74-33.196,74-74S478.804,182,438,182z M438,290H74			c-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34S456.748,290,438,290z" />				<path d="M438,364H74c-40.804,0-74,33.196-74,74s33.196,74,74,74h264c11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20H74			c-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34s-15.252,34-34,34c-11.046,0-20,8.954-20,20			c0,11.046,8.954,20,20,20c40.804,0,74-33.196,74-74S478.804,364,438,364z" /></>}
              </svg>
            </button>
          </div>
        </div>
        <div className={`px-2 pt-2 pb-4 ` + (isOpen ? 'block' : 'hidden') + ` sm:block sm:flex sm:py-0`}>
          <Link activeClass="active" to="header" spy={true} smooth={true} offset={0} duration={800} href="" className="menu-btn block px-2 py-1 font-regular cursor-pointer">
            Inicio
          </Link>
          <Link activeClass="active" to="beneficios" spy={true} smooth={true} offset={0} duration={800} className="menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
            Beneficios
          </Link>
          <Link activeClass="active" to="servicios" spy={true} smooth={true} offset={0} duration={800} className="menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
            Servicios
          </Link>
            <Link activeClass="active" to="diseno-grafico" spy={true} smooth={true} offset={0} duration={800} className="menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
              Diseño gráfico
            </Link>
            <Link activeClass="active" to="fotografia" spy={true} smooth={true} offset={0} duration={800} className="menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
              Fotografía
            </Link>
            <Link activeClass="active" to="promocion-digital" spy={true} smooth={true} offset={0} duration={800} className="menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
              Promoción digital
            </Link>
            <Link activeClass="active" to="diseno-web" spy={true} smooth={true} offset={0} duration={800} className="menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
              Diseño web
            </Link>
            <Link activeClass="active" to="informatica" spy={true} smooth={true} offset={0} duration={800} className="menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
              Informática
            </Link>
          <Link activeClass="active" to="contactos" spy={true} smooth={true} offset={0} duration={800} className="menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer">
            Contactos
          </Link>
        </div>
      </header>
      <style jsx>{`
        .navbar-ticky {
          animation: navbarMoveDown 0.5s ease-in-out
        }

        .navbar-absolute {
          animation: navbarMoveUp 0.5s ease-in-out
        }

        @keyframes navbarMoveDown {
          from {
            transform: translateY(-5rem);
          }
          to {
            transform: translateY(0rem);
          }
        }
      `}</style>
    </>
  )
}