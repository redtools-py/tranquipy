import styled from 'styled-components'

export const Slide = styled.div`
  width: ${props => (props.width ? props.width + 'px' : '100vw')};
  height: auto;
  display: block;
  visibility: ${props => (props.zooming ? (props.isCurrent ? 'visible' : 'hidden') : 'visible')}};
`