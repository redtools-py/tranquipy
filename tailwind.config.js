module.exports = {
  purge: ['./components/**/*.js', './pages/**/*.js'],
  theme: {
    extend: {
      colors: {
        'blue': {
          100: '#ECF5FF',
          200: '#CFE6FF',
          300: '#B3D7FF',
          400: '#79BAFF',
          500: '#409CFF',
          600: '#3A8CE6',
          700: '#265E99',
          800: '#1D4673',
          900: '#132F4D',
        }
      }
    },
  },
  variants: {},
  plugins: [
    require('tailwindcss-animatecss')({
      settings: {
        animatedSpeed: 1000,
        heartBeatSpeed: 1000,
        hingeSpeed: 2000,
        bounceInSpeed: 750,
        bounceOutSpeed: 750,
        animationDelaySpeed: 1000
      },
      variants: ['responsive', 'hover', 'reduced-motion'],
    })
  ],
}