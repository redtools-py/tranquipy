import '../styles/global.css'
import 'react-image-lightbox/style.css'
import ReactGA from 'react-ga'

export default function App({ Component, pageProps }) {
  if (process.browser) {
    ReactGA.initialize('UA-168322134-1');
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  return <Component {...pageProps} />
}