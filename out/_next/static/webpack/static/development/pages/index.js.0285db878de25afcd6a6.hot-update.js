webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/servicios/DisenoGrafico.js":
/*!***********************************************!*\
  !*** ./components/servicios/DisenoGrafico.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DisenoGrafico; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../lib/react-instagram-zoom-slider */ "./lib/react-instagram-zoom-slider/index.js");
var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\components\\servicios\\DisenoGrafico.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function DisenoGrafico() {
  var _this = this;

  var funcionalidades1SlidesData = [{
    url: 'images/portafolios_0002s_0001_Logos.jpg',
    title: 'Logos'
  }, {
    url: 'images/portafolios_0002s_0000_Fanpage.jpg',
    title: 'Diseños para social media'
  }, {
    url: 'images/portafolios_0002s_0000s_0000_Flyers 1.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0001_Flyers 2.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0002_Flyers 3.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0003_Flyers 4.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0004_Flyers 5.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0005_Flyers 6.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0006_Flyers 7.jpg',
    title: 'Diseños de Flyers'
  }, {
    url: 'images/portafolios_0002s_0000s_0007_Flyers 8.jpg',
    title: 'Diseños de Flyers'
  }];
  var funcionalidades1Slides = funcionalidades1SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 72
      }
    });
  });
  return __jsx("section", {
    id: "diseno-grafico",
    className: "py-20 lg:py-32 bg-orange-100",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 9
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_icon_design_qvdf-aqua-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "pt-5 text-4xl lg:text-5xl font-thin leading-tight text-aquamarina-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 11
    }
  }, "Dise\xF1o gr\xE1fico"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 11
    }
  }, "La comunicaci\xF3n visual con un mensaje claro es indispensable para competir en el mercado digital y desmarcarte ante tu p\xFAblico."))), __jsx("div", {
    className: "mt-16 lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M511.433,142.874c-5.522-51.601-34.546-81.534-65.09-106.192c-42.93-34.659-97.487-36.598-99.788-36.664  c-7.236-0.188-13.985,3.495-17.707,9.682c-3.722,6.188-3.821,13.899-0.258,20.179c14.932,26.319,7.981,45.652-0.821,70.13  c-6.672,18.555-14.233,39.586-10.98,64.083c3.524,26.522,16.608,50.006,36.843,66.122c0.445,0.354,0.903,0.686,1.352,1.031v60.23  c-27.258,1.593-47.946,7.215-62.923,17.062c-18.365,12.076-28.073,30.562-28.073,53.459c0,16.314,4.907,29.635,9.237,41.387  c4.165,11.307,7.763,21.073,7.763,33.61c0,5.465-0.95,15.808-7.309,23.682c-6.066,7.509-16.39,11.316-30.69,11.316  c-115.724,0-202.991-92.855-202.991-215.991c0-119.098,96.893-215.991,215.991-215.991c4.572,0,9.199,0.145,13.753,0.43  c11.022,0.684,20.519-7.688,21.209-18.711c0.69-11.023-7.687-20.519-18.71-21.209c-5.383-0.337-10.851-0.508-16.252-0.508  c-68.377,0-132.661,26.628-181.011,74.977C26.628,123.339,0,187.623,0,256c0,68.946,24.124,133.028,67.93,180.441  c45.01,48.718,107.181,75.548,175.06,75.548c33.605,0,52.159-14.239,61.806-26.184c10.441-12.925,16.19-30.262,16.19-48.813  c0-19.669-5.435-34.421-10.228-47.435c-3.783-10.27-6.771-18.38-6.771-27.562c0-9.539,2.912-15.344,10.051-20.038  c6.211-4.084,18.18-8.808,40.947-10.402v118.005c-0.65,15.645,4.886,30.777,15.62,42.663  c10.732,11.88,25.448,18.873,41.438,19.687c1.043,0.053,2.082,0.079,3.118,0.079c14.858,0,28.998-5.428,40.105-15.459  c11.881-10.731,18.874-25.447,19.687-41.437c0.561-11.031-7.927-20.428-18.957-20.99c-11.039-0.56-20.428,7.928-20.99,18.957  c-0.27,5.32-2.597,10.217-6.55,13.787c-3.953,3.57-9.051,5.393-14.381,5.116c-5.32-0.271-10.217-2.597-13.786-6.55  c-2.145-2.374-5.688-7.399-5.333-14.38l0.026-0.508V249.309c6.154,1.204,12.453,1.835,18.836,1.835  c3.533,0,7.094-0.187,10.661-0.567c3.554-0.377,7.056-0.947,10.501-1.691v110.109c0,11.045,8.954,19.999,19.999,19.999  s19.999-8.954,19.999-19.999V229.963c5.579-4.444,10.711-9.531,15.299-15.214C506.702,194.406,514.215,168.882,511.433,142.874z   M459.157,189.623c-9.709,12.024-23.524,19.547-38.9,21.18c-15.245,1.617-30.055-2.599-41.702-11.877  c-12.105-9.641-19.959-23.883-22.114-40.102c-1.973-14.851,3.097-28.954,8.969-45.282c6.901-19.19,14.989-41.688,10.755-68.113  c14.008,3.949,30.635,10.736,45.053,22.376c28.352,22.89,46.68,44.144,50.444,79.327  C473.307,162.507,468.866,177.597,459.157,189.623z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 17
    }
  }), "  ", __jsx("path", {
    d: "M223.99,110.006c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999  c0-16.541-13.457-29.999-29.999-29.999C237.448,80.008,223.99,93.465,223.99,110.006z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 2320
    }
  }), "  ", __jsx("path", {
    d: "M169.218,179.226c11.701-11.702,11.701-30.743,0-42.444l-0.001-0.001c-11.703-11.701-30.744-11.701-42.446,0.001  c-11.701,11.702-11.701,30.743,0.001,42.445c5.851,5.851,13.536,8.776,21.222,8.776S163.366,185.077,169.218,179.226z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 2508
    }
  }), "  ", __jsx("path", {
    d: "M79.997,263c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999s-13.457-29.999-29.999-29.999  C93.454,233.001,79.997,246.459,79.997,263z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 2746
    }
  }), "  ", __jsx("path", {
    d: "M150.994,391.994c8.013,0,15.545-3.121,21.212-8.789c5.666-5.667,8.786-13.202,8.786-21.218  c0-8.015-3.12-15.55-8.787-21.219c-11.698-11.698-30.728-11.696-42.423,0l-0.001,0.001c-5.666,5.667-8.786,13.202-8.786,21.218  c0,8.016,3.12,15.55,8.786,21.218C135.447,388.873,142.98,391.994,150.994,391.994z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 2919
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 17
    }
  }, "Dise\xF1o:"), " Creamos una identidad visual b\xE1sica para tu marca, logo, esquema de colores, fuentes y estilo gr\xE1fico.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m401.738281 442.710938c6.046875 9.242187 3.453125 21.636718-5.792969 27.683593-41.585937 27.195313-89.9375 41.582031-139.824218 41.605469-.039063 0-.082032 0-.121094 0-68.097656.003906-132.101562-26.234375-180.253906-73.890625-48.183594-47.683594-75.078125-111.46875-75.7343752-179.597656-.6601568-68.558594 25.4453122-133.210938 73.5039062-182.054688 48.066406-48.851562 112.265625-76 180.773437-76.45312475.5625 0 1.136719-.00390625 1.699219-.00390625 53.46875 0 104.816407 16.546875 148.613281 47.90625 43.277344 30.988281 75.519532 73.652344 93.238282 123.378906 3.707031 10.40625-1.722656 21.847656-12.125 25.554688-10.40625 3.707031-21.84375-1.722656-25.554688-12.125-30.898437-86.707032-112.84375-144.714844-204.195312-144.714844-.476563 0-.9375 0-1.414063.003906-57.796875.378906-111.964843 23.289063-152.523437 64.507813-40.546875 41.210937-62.570313 95.765625-62.015625 153.613281 1.132812 117.949219 98.019531 213.875 215.992187 213.875h.097656c42.097657-.019531 82.886719-12.148438 117.953126-35.082031 9.242187-6.046875 21.636718-3.453125 27.683593 5.792969zm110.261719-165.710938c0 50.179688-40.820312 91-91 91-32.0625 0-60.304688-16.667969-76.519531-41.796875-20.71875 26.058594-52.679688 42.796875-88.480469 42.796875-62.308594 0-113-50.691406-113-113s50.691406-113 113-113c28.277344 0 54.160156 10.441406 74 27.667969v-3.535157c0-11.042968 8.953125-20 20-20s20 8.957032 20 20v109.867188c0 28.121094 22.878906 51 51 51s51-22.878906 51-51c0-11.046875 8.953125-20 20-20s20 8.953125 20 20zm-183-21c0-40.25-32.746094-73-73-73-40.25 0-73 32.75-73 73 0 40.253906 32.75 73 73 73 40.253906 0 73-32.746094 73-73zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 17
    }
  }, "Social Media:"), " Perfil, portada, gr\xE1ficas para publicaciones e historias.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m312 256c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20s8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20zm-20 57h-192c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h192c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm-80 80h-112c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h112c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm260-61v-56h-80v37h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20v39h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20c0 22.054688 17.945312 40 40 40 22.097656 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.847656 80-79.910156 80-.246094 0-.484375 0-.726563-.003906-.121093.003906-.242187.003906-.363281.003906h-351c-44.113281 0-80-35.886719-80-80v-412c0-11.046875 8.953125-20 20-20h352c11.046875 0 20 8.953125 20 20v216h100c11.046875 0 20 8.953125 20 20v76c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20zm-109.261719 140c-6.828125-11.773438-10.738281-25.4375-10.738281-40v-392h-312v392c0 22.054688 17.945312 40 40 40zm-282.738281-293v-80c0-11.046875 8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20v80c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20zm40-20h151v-40h-151zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90,
      columnNumber: 17
    }
  }, "Papeler\xEDa:"), " Membretes, tarjetas personales, banners y m\xE1s.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94,
      columnNumber: 13
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 95,
      columnNumber: 15
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-aquamarina-claro hover:bg-teal-400 active:bg-teal-400 text-blue-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 21
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 107,
      columnNumber: 11
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__["default"], {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112,
          columnNumber: 24
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 108,
      columnNumber: 13
    }
  })))), ".");
}

/***/ })

})
//# sourceMappingURL=index.js.0285db878de25afcd6a6.hot-update.js.map