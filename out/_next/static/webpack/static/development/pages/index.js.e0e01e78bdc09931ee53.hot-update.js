webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/servicios/Fotografia.js":
/*!********************************************!*\
  !*** ./components/servicios/Fotografia.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Fotografia; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../lib/react-instagram-zoom-slider */ "./lib/react-instagram-zoom-slider/index.js");
var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\components\\servicios\\Fotografia.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Fotografia() {
  var _this = this;

  var funcionalidades1SlidesData = [{
    url: 'images/portafolios_0001s_0000s_0000_Corporativo 1.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0000_Productos 1.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0001_Corporativo 2.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0001_Productos 2.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0002_Corporativo 3.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0002_Productos 3.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0003_Corporativo 4.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0003_Productos 4.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0003_Corporativo 5.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0003_Productos 5.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0003_Corporativo 6.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0003_Productos 6.jpg',
    title: 'Fotografía de productos'
  }, {
    url: 'images/portafolios_0001s_0000s_0003_Corporativo 4.jpg',
    title: 'Fotografía corporativa'
  }, {
    url: 'images/portafolios_0001s_0001s_0003_Productos 4.jpg',
    title: 'Fotografía de productos'
  }];
  var funcionalidades1Slides = funcionalidades1SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62,
        columnNumber: 72
      }
    });
  });
  return __jsx("section", {
    id: "fotografia",
    className: "py-20 lg:py-32",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 lg:pl-16 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 9
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_product_photography_91i2-limon.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "pt-6 text-4xl lg:text-5xl font-thin leading-tight text-green-600",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 11
    }
  }, "Fotograf\xEDa"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 11
    }
  }, "Cr\xE9a un v\xEDnculo entre tu p\xFAblico y tu empresa despertando su inter\xE9s y confianza."))), __jsx("div", {
    className: "mt-16 lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-20 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m432 462h-352c-44.113281 0-80-35.886719-80-80v-230c0-44.113281 35.886719-80 80-80h81.773438c4.277343 0 8.167968-2.527344 9.90625-6.433594l15.761718-35.402344c8.160156-18.324218 26.394532-30.164062 46.453125-30.164062h115.289063c19.722656 0 37.828125 11.558594 46.125 29.445312l16.828125 36.273438c1.769531 3.8125 5.632812 6.28125 9.839843 6.28125h10.023438c44.113281 0 80 35.886719 80 80v130c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20v-130c0-22.054688-17.945312-40-40-40h-10.023438c-19.722656 0-37.824218-11.558594-46.125-29.445312l-16.828124-36.273438c-1.769532-3.816406-5.632813-6.28125-9.839844-6.28125h-115.289063c-4.28125 0-8.167969 2.527344-9.910156 6.433594l-15.761719 35.402344c-8.160156 18.324218-26.394531 30.164062-46.449218 30.164062h-81.773438c-22.054688 0-40 17.945312-40 40v230c0 22.054688 17.945312 40 40 40h352c22.054688 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.886719 80-80 80zm-12-205c0-68.925781-56.074219-125-125-125s-125 56.074219-125 125 56.074219 125 125 125 125-56.074219 125-125zm-40 0c0 46.867188-38.132812 85-85 85s-85-38.132812-85-85 38.132812-85 85-85 85 38.132812 85 85zm-285 4c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20-20 8.953125-20 20 8.953125 20 20 20zm0-70c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20-20 8.953125-20 20 8.953125 20 20 20zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-green-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 87,
      columnNumber: 17
    }
  }, "Fotograf\xEDa de productos: "), "Resalt\xE1 las caracter\xEDsticas principales de tus productos.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 92,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 93,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m432 0h-352c-44.113281 0-80 35.886719-80 80v280c0 44.113281 35.886719 80 80 80h167c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20h-167c-10.230469 0-19.570312-3.863281-26.648438-10.203125l87.84375-107.632813 52.316407 54.621094c.007812.011719.015625.019532.023437.027344l.019532.023438c.335937.347656.6875.660156 1.042968.980468.140625.125.265625.265625.414063.390625 0 0 .003906.003907.003906.003907 1.558594 1.332031 3.273437 2.390624 5.082031 3.167968.296875.125.597656.222656.898438.335938.304687.113281.609375.238281.917968.339844.402344.128906.816407.222656 1.226563.328124.210937.050782.421875.121094.640625.167969.402344.085938.8125.140625 1.21875.203125.226562.03125.453125.082032.683594.109375.355468.042969.710937.050781 1.066406.074219.285156.019531.574219.050781.859375.054688.28125.007812.558594-.011719.839844-.019532.363281-.007812.730469-.007812 1.089843-.035156.203126-.011719.398438-.050781.597657-.070312.441406-.046876.882812-.09375 1.320312-.167969.132813-.023438.265625-.0625.398438-.089844.496093-.097656.992187-.199219 1.480469-.332031.109374-.03125.214843-.074219.320312-.105469.507812-.148437 1.015625-.308594 1.515625-.496094.164063-.066406.324219-.148437.492187-.21875.429688-.175781.859376-.351562 1.277344-.5625.566406-.28125 1.117188-.59375 1.65625-.929687.015625-.011719.03125-.019532.046875-.027344 1.125-.707031 2.191407-1.535156 3.179688-2.484375.003906-.003906.007812-.007813.011719-.007813.347656-.335937.660156-.6875.980468-1.042968.128906-.140625.265625-.265625.390625-.414063l.007813-.007812c.015625-.019531.03125-.035157.046875-.054688l154.753906-181.152343 79.789063 93.230468c3.800781 4.441406 9.351562 6.996094 15.195312 6.996094h19c11.046875 0 20-8.953125 20-20v-155c0-44.113281-35.886719-80-80-80zm40 203.074219-78.804688-92.078125c-3.800781-4.441406-9.351562-6.996094-15.195312-6.996094-.003906 0-.007812 0-.007812 0-5.847657.003906-11.402344 2.5625-15.199219 7.011719l-155.640625 182.1875-52.707032-55.035157c-3.964843-4.136718-9.488281-6.375-15.238281-6.148437-5.726562.226563-11.078125 2.898437-14.699219 7.339844l-84.507812 103.535156v-262.890625c0-22.054688 17.945312-40 40-40h352c22.054688 0 40 17.945312 40 40zm-332-131.074219c-33.085938 0-60 26.914062-60 60s26.914062 60 60 60 60-26.914062 60-60-26.914062-60-60-60zm0 80c-11.027344 0-20-8.972656-20-20s8.972656-20 20-20 20 8.972656 20 20-8.972656 20-20 20zm304.5 126c-19.105469 0-35.492188 6.308594-47.5 18.035156-12.007812-11.726562-28.394531-18.035156-47.5-18.035156-18.445312 0-35.429688 6.90625-47.816406 19.445312-12.695313 12.851563-19.683594 30.949219-19.683594 50.960938 0 42.292969 36.351562 69.398438 65.5625 91.175781 13.5625 10.113281 26.375 19.664063 33.859375 28.960938 3.796875 4.714843 9.523437 7.457031 15.578125 7.457031s11.78125-2.742188 15.578125-7.457031c7.484375-9.296875 20.296875-18.847657 33.859375-28.960938 29.210938-21.777343 65.5625-48.882812 65.5625-91.175781 0-20.011719-6.988281-38.109375-19.683594-50.960938-12.386718-12.539062-29.371094-19.445312-47.816406-19.445312zm-21.96875 129.511719c-8.6875 6.476562-17.542969 13.082031-25.53125 20.070312-7.988281-6.988281-16.84375-13.59375-25.53125-20.070312-24.320312-18.132813-49.46875-36.882813-49.46875-59.105469 0-18.46875 10.792969-30.40625 27.5-30.40625 14.152344 0 19.722656 5.980469 22.679688 10.476562 3.835937 5.832032 4.769531 12.238282 4.886718 13.175782.617188 10.464844 9.199219 18.4375 19.695313 18.570312h.253906c10.425781 0 19.109375-8.273437 19.925781-18.699218.003906-.054688.800782-6.335938 4.335938-12.1875 2.945312-4.867188 8.554687-11.335938 23.222656-11.335938 16.707031 0 27.5 11.9375 27.5 30.40625 0 22.222656-25.148438 40.972656-49.46875 59.105469zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 94,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 96,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-green-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 17
    }
  }, "Fotograf\xEDa corporativa: "), "Present\xE1 tu local, el equipo de trabajo, los procesos de la empresa y las dem\xE1s \xE1reas de inter\xE9s.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 102,
      columnNumber: 13
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 103,
      columnNumber: 15
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-limon hover:bg-green-400 active:bg-green-400 text-green-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 105,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 106,
      columnNumber: 21
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 114,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg mx-auto service-slider",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 11
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__["default"], {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 120,
          columnNumber: 24
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 116,
      columnNumber: 13
    }
  })))));
}

/***/ })

})
//# sourceMappingURL=index.js.e0e01e78bdc09931ee53.hot-update.js.map