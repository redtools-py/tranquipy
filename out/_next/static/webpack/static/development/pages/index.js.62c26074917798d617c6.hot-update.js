webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/servicios/DisenoWeb.js":
/*!*******************************************!*\
  !*** ./components/servicios/DisenoWeb.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DisenoWeb; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../lib/react-instagram-zoom-slider */ "./lib/react-instagram-zoom-slider/index.js");
var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\components\\servicios\\DisenoWeb.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function DisenoWeb() {
  var _this = this;

  var funcionalidades1SlidesData = [{
    url: 'images/portafolios_0000s_0000_Diseño Adaptable.jpg',
    title: 'Diseño adaptable a móviles'
  }, {
    url: 'images/portafolios_0000s_0001_Detalle de productos.jpg',
    title: 'Imágen 2'
  }, {
    url: 'images/portafolios_0000s_0001_Detalle de productos.jpg',
    title: 'Imágen 2'
  }, {
    url: 'images/portafolios_0000s_0001_Detalle de productos.jpg',
    title: 'Imágen 2'
  }];
  var funcionalidades1Slides = funcionalidades1SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 72
      }
    });
  });
  return __jsx("section", {
    id: "diseno-web",
    className: "py-20 lg:py-32",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 lg:pl-16 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 9
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_add_to_cart_vkjp-celeste-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "pt-6 text-4xl lg:text-5xl font-thin leading-tight text-celeste-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 11
    }
  }, "Dise\xF1o web"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 11
    }
  }, "Gu\xEDa a tus potenciales clientes en el proceso de venta mostrando lo que ofreces y qui\xE9n eres de manera eficiente. Ten el control de tu informaci\xF3n en internet."))), __jsx("div", {
    className: "mt-16 lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-20 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m452 0h-392c-33.085938 0-60 26.914062-60 60v332c0 44.113281 35.886719 80 80 80h352c44.113281 0 80-35.886719 80-80 0-11.046875-8.953125-20-20-20s-20 8.953125-20 20c0 22.054688-17.945312 40-40 40h-352c-22.054688 0-40-17.945312-40-40v-271h432v171c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-232c0-33.085938-26.914062-60-60-60zm-81 40c11.027344 0 20 8.972656 20 20s-8.972656 20-20 20-20-8.972656-20-20 8.972656-20 20-20zm100 20c0 11.027344-8.972656 20-20 20s-20-8.972656-20-20 8.972656-20 20-20 20 8.972656 20 20zm-431 0c0-11.027344 8.972656-20 20-20h254.441406c-2.222656 6.261719-3.441406 12.988281-3.441406 20 0 7.386719 1.347656 14.460938 3.800781 21h-274.800781zm216 138.042969c-12.007812-11.730469-28.394531-18.042969-47.5-18.042969-18.398438 0-35.347656 6.8125-47.726562 19.1875-12.75 12.742188-19.773438 30.707031-19.773438 50.582031 0 41.960938 36.40625 68.761719 65.65625 90.296875 13.5625 9.980469 26.371094 19.410156 33.84375 28.574219 3.800781 4.65625 9.488281 7.359375 15.5 7.359375s11.699219-2.703125 15.5-7.359375c7.472656-9.164063 20.28125-18.59375 33.84375-28.574219 29.25-21.535156 65.65625-48.335937 65.65625-90.296875 0-19.875-7.023438-37.839843-19.773438-50.582031-12.378906-12.375-29.328124-19.1875-47.726562-19.1875-19.105469 0-35.492188 6.3125-47.5 18.042969zm75 51.726562c0 21.738281-25.097656 40.214844-49.371094 58.082031-8.71875 6.417969-17.613281 12.964844-25.628906 19.902344-8.015625-6.9375-16.910156-13.484375-25.628906-19.902344-24.273438-17.867187-49.371094-36.34375-49.371094-58.082031 0-18.082031 10.792969-29.769531 27.5-29.769531 14.148438 0 19.71875 5.984375 22.675781 10.484375 3.839844 5.84375 4.773438 12.261719 4.890625 13.199219.621094 10.5625 9.339844 18.820312 19.933594 18.820312 10.597656 0 19.3125-8.261718 19.933594-18.820312.117187-.9375 1.050781-7.355469 4.890625-13.199219 2.957031-4.5 8.527343-10.484375 22.675781-10.484375 16.707031 0 27.5 11.6875 27.5 29.769531zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 17
    }
  }, "Landing pages:"), " Capta m\xE1s interesados en tus productos y servicios con una p\xE1gina web optimizada para venta.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M507.519,116.384C503.721,111.712,498.021,109,492,109H129.736l-1.484-13.632l-0.053-0.438C121.099,40.812,74.583,0,20,0 C8.954,0,0,8.954,0,20s8.954,20,20,20c34.506,0,63.923,25.749,68.512,59.928l23.773,218.401C91.495,327.765,77,348.722,77,373 c0,0.167,0.002,0.334,0.006,0.5C77.002,373.666,77,373.833,77,374c0,33.084,26.916,60,60,60h8.138 c-2.034,5.964-3.138,12.355-3.138,19c0,32.532,26.467,59,59,59s59-26.468,59-59c0-6.645-1.104-13.036-3.138-19h86.277 c-2.034,5.964-3.138,12.355-3.138,19c0,32.532,26.467,59,59,59c32.533,0,59-26.468,59-59c0-32.532-26.467-59-59-59H137 c-11.028,0-20-8.972-20-20c0-0.167-0.002-0.334-0.006-0.5c0.004-0.166,0.006-0.333,0.006-0.5c0-11.028,8.972-20,20-20h255.331 c35.503,0,68.084-21.966,83.006-55.962c4.439-10.114-0.161-21.912-10.275-26.352c-10.114-4.439-21.912,0.161-26.352,10.275 C430.299,300.125,411.661,313,392.331,313h-240.39L134.09,149h333.308l-9.786,46.916c-2.255,10.813,4.682,21.407,15.495,23.662 c1.377,0.288,2.75,0.426,4.104,0.426c9.272,0,17.59-6.484,19.558-15.92l14.809-71C512.808,127.19,511.317,121.056,507.519,116.384 z M399,434c10.477,0,19,8.523,19,19s-8.523,19-19,19s-19-8.523-19-19S388.523,434,399,434z M201,434c10.477,0,19,8.524,19,19 c0,10.477-8.523,19-19,19s-19-8.523-19-19S190.523,434,201,434z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 17
    }
  }, "eCommerce:"), " Muestra tu cat\xE1logo de productos y automatiza tu recepci\xF3n de pedidos con un sistema de venta online.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 13
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 15
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-celeste-oscuro hover:bg-teal-400 active:bg-teal-400 text-white text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 21
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg mx-auto service-slider",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 75,
      columnNumber: 11
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_1__["default"], {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 24
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 13
    }
  })))));
}

/***/ })

})
//# sourceMappingURL=index.js.62c26074917798d617c6.hot-update.js.map