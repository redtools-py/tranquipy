webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/servicios/graphic-design.js":
/*!************************************************!*\
  !*** ./components/servicios/graphic-design.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return graphicDesign; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\components\\servicios\\graphic-design.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
function graphicDesign() {
  var _this = this;

  var funcionalidades1SlidesData = [{
    url: 'images/balazs-ketyi-f5T3eyxUQg8-unsplash.jpg',
    title: 'Imágen 1'
  }, {
    url: 'images/georgie-cobbs-i3PakNjFfAc-unsplash.jpg',
    title: 'Imágen 2'
  }];
  var funcionalidades1Slides = funcionalidades1SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 72
      }
    });
  });
  return __jsx("section", {
    id: "diseno-grafico",
    className: "py-20 lg:py-32 bg-orange-100",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 5
    }
  }, __jsx("div", {
    className: "lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_icon_design_qvdf-aqua-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 11
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 9
    }
  }, __jsx("h3", {
    className: "pt-5 text-4xl lg:text-5xl font-thin leading-tight text-aquamarina-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 11
    }
  }, "Dise\xF1o gr\xE1fico"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 11
    }
  }, "La comunicaci\xF3n visual con un mensaje claro es indispensable para competir en el mercado digital y desmarcarte ante tu p\xFAblico."))), __jsx("div", {
    className: "mt-16 lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M511.433,142.874c-5.522-51.601-34.546-81.534-65.09-106.192c-42.93-34.659-97.487-36.598-99.788-36.664  c-7.236-0.188-13.985,3.495-17.707,9.682c-3.722,6.188-3.821,13.899-0.258,20.179c14.932,26.319,7.981,45.652-0.821,70.13  c-6.672,18.555-14.233,39.586-10.98,64.083c3.524,26.522,16.608,50.006,36.843,66.122c0.445,0.354,0.903,0.686,1.352,1.031v60.23  c-27.258,1.593-47.946,7.215-62.923,17.062c-18.365,12.076-28.073,30.562-28.073,53.459c0,16.314,4.907,29.635,9.237,41.387  c4.165,11.307,7.763,21.073,7.763,33.61c0,5.465-0.95,15.808-7.309,23.682c-6.066,7.509-16.39,11.316-30.69,11.316  c-115.724,0-202.991-92.855-202.991-215.991c0-119.098,96.893-215.991,215.991-215.991c4.572,0,9.199,0.145,13.753,0.43  c11.022,0.684,20.519-7.688,21.209-18.711c0.69-11.023-7.687-20.519-18.71-21.209c-5.383-0.337-10.851-0.508-16.252-0.508  c-68.377,0-132.661,26.628-181.011,74.977C26.628,123.339,0,187.623,0,256c0,68.946,24.124,133.028,67.93,180.441  c45.01,48.718,107.181,75.548,175.06,75.548c33.605,0,52.159-14.239,61.806-26.184c10.441-12.925,16.19-30.262,16.19-48.813  c0-19.669-5.435-34.421-10.228-47.435c-3.783-10.27-6.771-18.38-6.771-27.562c0-9.539,2.912-15.344,10.051-20.038  c6.211-4.084,18.18-8.808,40.947-10.402v118.005c-0.65,15.645,4.886,30.777,15.62,42.663  c10.732,11.88,25.448,18.873,41.438,19.687c1.043,0.053,2.082,0.079,3.118,0.079c14.858,0,28.998-5.428,40.105-15.459  c11.881-10.731,18.874-25.447,19.687-41.437c0.561-11.031-7.927-20.428-18.957-20.99c-11.039-0.56-20.428,7.928-20.99,18.957  c-0.27,5.32-2.597,10.217-6.55,13.787c-3.953,3.57-9.051,5.393-14.381,5.116c-5.32-0.271-10.217-2.597-13.786-6.55  c-2.145-2.374-5.688-7.399-5.333-14.38l0.026-0.508V249.309c6.154,1.204,12.453,1.835,18.836,1.835  c3.533,0,7.094-0.187,10.661-0.567c3.554-0.377,7.056-0.947,10.501-1.691v110.109c0,11.045,8.954,19.999,19.999,19.999  s19.999-8.954,19.999-19.999V229.963c5.579-4.444,10.711-9.531,15.299-15.214C506.702,194.406,514.215,168.882,511.433,142.874z   M459.157,189.623c-9.709,12.024-23.524,19.547-38.9,21.18c-15.245,1.617-30.055-2.599-41.702-11.877  c-12.105-9.641-19.959-23.883-22.114-40.102c-1.973-14.851,3.097-28.954,8.969-45.282c6.901-19.19,14.989-41.688,10.755-68.113  c14.008,3.949,30.635,10.736,45.053,22.376c28.352,22.89,46.68,44.144,50.444,79.327  C473.307,162.507,468.866,177.597,459.157,189.623z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 17
    }
  }), "  ", __jsx("path", {
    d: "M223.99,110.006c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999  c0-16.541-13.457-29.999-29.999-29.999C237.448,80.008,223.99,93.465,223.99,110.006z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 2320
    }
  }), "  ", __jsx("path", {
    d: "M169.218,179.226c11.701-11.702,11.701-30.743,0-42.444l-0.001-0.001c-11.703-11.701-30.744-11.701-42.446,0.001  c-11.701,11.702-11.701,30.743,0.001,42.445c5.851,5.851,13.536,8.776,21.222,8.776S163.366,185.077,169.218,179.226z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 2508
    }
  }), "  ", __jsx("path", {
    d: "M79.997,263c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999s-13.457-29.999-29.999-29.999  C93.454,233.001,79.997,246.459,79.997,263z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 2746
    }
  }), "  ", __jsx("path", {
    d: "M150.994,391.994c8.013,0,15.545-3.121,21.212-8.789c5.666-5.667,8.786-13.202,8.786-21.218  c0-8.015-3.12-15.55-8.787-21.219c-11.698-11.698-30.728-11.696-42.423,0l-0.001,0.001c-5.666,5.667-8.786,13.202-8.786,21.218  c0,8.016,3.12,15.55,8.786,21.218C135.447,388.873,142.98,391.994,150.994,391.994z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 2919
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 17
    }
  }, "Dise\xF1o:"), " Creamos una identidad visual b\xE1sica para tu marca, logo, esquema de colores, fuentes y estilo gr\xE1fico.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m401.738281 442.710938c6.046875 9.242187 3.453125 21.636718-5.792969 27.683593-41.585937 27.195313-89.9375 41.582031-139.824218 41.605469-.039063 0-.082032 0-.121094 0-68.097656.003906-132.101562-26.234375-180.253906-73.890625-48.183594-47.683594-75.078125-111.46875-75.7343752-179.597656-.6601568-68.558594 25.4453122-133.210938 73.5039062-182.054688 48.066406-48.851562 112.265625-76 180.773437-76.45312475.5625 0 1.136719-.00390625 1.699219-.00390625 53.46875 0 104.816407 16.546875 148.613281 47.90625 43.277344 30.988281 75.519532 73.652344 93.238282 123.378906 3.707031 10.40625-1.722656 21.847656-12.125 25.554688-10.40625 3.707031-21.84375-1.722656-25.554688-12.125-30.898437-86.707032-112.84375-144.714844-204.195312-144.714844-.476563 0-.9375 0-1.414063.003906-57.796875.378906-111.964843 23.289063-152.523437 64.507813-40.546875 41.210937-62.570313 95.765625-62.015625 153.613281 1.132812 117.949219 98.019531 213.875 215.992187 213.875h.097656c42.097657-.019531 82.886719-12.148438 117.953126-35.082031 9.242187-6.046875 21.636718-3.453125 27.683593 5.792969zm110.261719-165.710938c0 50.179688-40.820312 91-91 91-32.0625 0-60.304688-16.667969-76.519531-41.796875-20.71875 26.058594-52.679688 42.796875-88.480469 42.796875-62.308594 0-113-50.691406-113-113s50.691406-113 113-113c28.277344 0 54.160156 10.441406 74 27.667969v-3.535157c0-11.042968 8.953125-20 20-20s20 8.957032 20 20v109.867188c0 28.121094 22.878906 51 51 51s51-22.878906 51-51c0-11.046875 8.953125-20 20-20s20 8.953125 20 20zm-183-21c0-40.25-32.746094-73-73-73-40.25 0-73 32.75-73 73 0 40.253906 32.75 73 73 73 40.253906 0 73-32.746094 73-73zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 17
    }
  }, "Social Media:"), " Perfil, portada, gr\xE1ficas para publicaciones e historias.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m312 256c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20s8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20zm-20 57h-192c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h192c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm-80 80h-112c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h112c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm260-61v-56h-80v37h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20v39h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20c0 22.054688 17.945312 40 40 40 22.097656 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.847656 80-79.910156 80-.246094 0-.484375 0-.726563-.003906-.121093.003906-.242187.003906-.363281.003906h-351c-44.113281 0-80-35.886719-80-80v-412c0-11.046875 8.953125-20 20-20h352c11.046875 0 20 8.953125 20 20v216h100c11.046875 0 20 8.953125 20 20v76c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20zm-109.261719 140c-6.828125-11.773438-10.738281-25.4375-10.738281-40v-392h-312v392c0 22.054688 17.945312 40 40 40zm-282.738281-293v-80c0-11.046875 8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20v80c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20zm40-20h151v-40h-151zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 17
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 15
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56,
      columnNumber: 17
    }
  }, "Papeler\xEDa:"), " Membretes, tarjetas personales, banners y m\xE1s.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 13
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61,
      columnNumber: 15
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-aquamarina-claro hover:bg-teal-400 active:bg-teal-400 text-blue-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 21
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 11
    }
  }, __jsx(ZoomSlider, {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78,
          columnNumber: 24
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 13
    }
  })))));
}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: __N_SSG, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__N_SSG", function() { return __N_SSG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next/dist/next-server/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-scroll */ "./node_modules/react-scroll/modules/index.js");
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_scroll__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/layout */ "./components/layout.js");
/* harmony import */ var _lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../lib/react-instagram-zoom-slider */ "./lib/react-instagram-zoom-slider/index.js");
/* harmony import */ var _components_servicios_graphic_design__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/servicios/graphic-design */ "./components/servicios/graphic-design.js");
var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\pages\\index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



 // import useSticky from '../lib/hooks/useSticky'




__jsx("graphicDesign", {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 1
  }
});

var __N_SSG = true;
function Home(_ref) {
  var _this = this;

  var allPostsData = _ref.allPostsData;
  // const { isSticky, element } = useSticky()
  var funcionalidades1SlidesData = [{
    url: 'images/balazs-ketyi-f5T3eyxUQg8-unsplash.jpg',
    title: 'Imágen 1'
  }, {
    url: 'images/georgie-cobbs-i3PakNjFfAc-unsplash.jpg',
    title: 'Imágen 2'
  }];
  var funcionalidades1Slides = funcionalidades1SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 72
      }
    });
  });
  var funcionalidades2SlidesData = [{
    url: 'images/matt-ridley-MrVEedTZLwM-unsplash.jpg',
    title: 'Imágen 1'
  }, {
    url: 'images/jk-park-B6k7v0eIOYo-unsplash.jpg',
    title: 'Imágen 2'
  }];
  var funcionalidades2Slides = funcionalidades2SlidesData.map(function (src) {
    return __jsx("img", {
      src: src.url,
      alt: src.title,
      draggable: "false",
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 36,
        columnNumber: 72
      }
    });
  });
  return __jsx(_components_layout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    home: true,
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 5
    }
  }, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 7
    }
  }, __jsx("title", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 9
    }
  }, _components_layout__WEBPACK_IMPORTED_MODULE_3__["siteTitle"])), __jsx("header", {
    id: "header",
    className: "header-bg-lg lg:h-screen lg:flex lg:items-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "h-screen lg:max-w-screen-lg xl:max-w-screen-xl mx-auto lg:flex lg:items-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "px-10 pt-32 pb-24 lg:pt-20 lg:pb-40 header-bg text-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 11
    }
  }, __jsx("h1", {
    className: "mt-2 lg:mb-10",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "h-64 w-auto mx-auto",
    src: "logo-lema-limon.svg",
    alt: "Tranqui | Dise\xF1o e Inform\xE1tica",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48,
      columnNumber: 15
    }
  })), __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 13
    }
  }, __jsx("button", {
    className: "mb-8 lg:mb-20 px-5 py-2 bg-celeste-oscuro hover:bg-teal-500 active:bg-teal-500 text-white rounded-lg shadow lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 15
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 17
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 19
    }
  })), "Contactar")), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    to: "lema",
    smooth: true,
    offset: 0,
    duration: 800,
    href: "",
    className: "block",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 65,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "mx-auto fill-current text-limon w-10 cursor-pointer animate__animated animate__fadeInDown animate__delay-1s",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M512.4,163.8c0,0.1,0,0.2,0,0.3c-0.1,16.1-6.5,31.2-18,42.5L297.7,400.2c-11.3,11.1-26.2,17.2-42,17.2\tc-15.8,0-30.7-6-42-17.1L18.4,207.9c-11.6-11.3-18-26.5-18-42.7c0-16.3,6.3-31.5,17.9-42.9l9.4-9.3c11.4-11.3,26.5-17.4,42.6-17.3\ts31.1,6.5,42.3,17.9l143.3,146.2l41.2-42.1c7.7-7.9,20.4-8,28.3-0.3c7.9,7.7,8,20.4,0.3,28.3l-55.5,56.7c-3.8,3.8-8.9,6-14.3,6\tc-5.4,0-10.5-2.2-14.3-6L84,141.6c-3.7-3.8-8.7-5.9-14.1-5.9c-5.3,0-10.4,2-14.2,5.8l-9.4,9.3c-3.9,3.8-6,8.9-6,14.3\tc0,5.4,2.2,10.5,6,14.3l195.4,192.4c7.7,7.5,20.2,7.5,27.9-0.1l196.7-193.5c3.9-3.8,6-8.8,6-14.2c0-5.4-2.1-10.4-5.9-14.2l-9.2-9.2\tc-3.8-3.8-8.9-5.9-14.3-5.9c-5.4,0-10.4,2.2-14.2,6l-35.2,35.9c-7.7,7.9-20.4,8-28.3,0.3c-7.9-7.7-8-20.4-0.3-28.3l35.2-35.9\tc11.3-11.5,26.4-17.9,42.6-18c16.1-0.1,31.3,6.2,42.8,17.6l9.2,9.1C506.1,132.7,512.4,147.8,512.4,163.8z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 66,
      columnNumber: 162
    }
  })))))), __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 7
    }
  }, __jsx("img", {
    src: "color-palette.svg",
    className: "h-12 -mt-12 lg:h-20 lg:-mt-20 w-screen object-cover relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 72,
      columnNumber: 9
    }
  })), __jsx("section", {
    id: "lema",
    className: "py-20 px-10 lg:py-32 text-center bg-aquamarina-oscuro leading-tight",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 7
    }
  }, __jsx("img", {
    className: "mx-auto mb-10 h-48 lg:h-64",
    src: "vectors/undraw_happy_announcement_ac67-blue.svg",
    alt: "Empieza tu camino hacia arriba",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 9
    }
  }), __jsx("h2", {
    className: "text-white font-black text-3xl lg:text-5xl uppercase",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 9
    }
  }, "Empieza tu camino hacia arriba"), __jsx("h3", {
    className: "text-white font-thin text-xl lg:text-3xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 9
    }
  }, "Para emprendedores/as, micro y peque\xF1as empresas.")), __jsx("section", {
    id: "beneficios",
    className: "px-6 mt-0 py-20 sm:px-20 lg:py-32",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "lg:max-w-screen-lg xl:max-w-screen-xl lg:flex lg:align-middle mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "text-center lg:mx-8 lg:flex-1",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84,
      columnNumber: 11
    }
  }, __jsx("svg", {
    className: "fill-current text-limon w-auto h-12 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 13
    }
  }, __jsx("path", {
    d: "M45.33,283.02l-39.696,40.92c-7.675,7.913-7.484,20.55,0.429,28.226c3.878,3.763,8.889,5.634,13.897,5.634\t\t\t\tc5.21,0,10.416-2.028,14.329-6.062l39.696-40.92c7.675-7.913,7.484-20.55-0.429-28.226\t\t\t\tC65.643,274.915,53.006,275.107,45.33,283.02z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 15
    }
  }), "   ", __jsx("path", {
    d: "M137.731,295.92c-11.023,0-19.961,8.937-19.961,19.961v175.658c0,11.023,8.937,19.961,19.961,19.961h0.001\t\t\t\tc11.023,0,19.961-8.937,19.961-19.961l-0.002-175.659C157.691,304.857,148.755,295.92,137.731,295.92z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 268
    }
  }), "   ", __jsx("path", {
    d: "M215.579,252.007c-11.023,0-19.961,8.937-19.961,19.961v219.571c0,11.023,8.937,19.961,19.961,19.961\t\t\t\ts19.961-8.937,19.961-19.961V271.967C235.539,260.944,226.603,252.007,215.579,252.007z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 488
    }
  }), "   ", __jsx("path", {
    d: "M452.116,0.498h-59.883c-11.024,0-19.961,8.937-19.961,19.961s8.937,19.961,19.961,19.961h52.61L298.273,186.959\t\t\t\tc-3.932,3.933-9.078,6.17-14.167,6.17c-0.107,0-0.216-0.001-0.323-0.003c-4.901-0.092-9.91-2.339-13.738-6.168l-23.468-23.468\t\t\t\tc-11.489-11.489-26.741-18.149-41.848-18.273c-0.147-0.001-0.293-0.001-0.439-0.001c-15.636,0-31.25,6.835-42.919,18.799\t\t\t\tl-42.776,42.969c-7.778,7.813-7.75,20.451,0.063,28.229c3.894,3.877,8.988,5.815,14.082,5.815c5.123,0,10.246-1.961,14.146-5.879\t\t\t\tl42.854-43.046c0.059-0.058,0.116-0.117,0.174-0.176c4.168-4.293,9.6-6.839,14.488-6.79c4.651,0.038,9.865,2.498,13.948,6.581\t\t\t\tl23.468,23.468c11.226,11.227,25.866,17.568,41.223,17.854c15.969,0.3,31.822-6.209,43.463-17.852L472.077,69.644v49.622\t\t\t\tc0,11.024,8.937,19.961,19.961,19.961s19.961-8.937,19.961-19.961V60.381C511.999,27.362,485.136,0.498,452.116,0.498z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 689
    }
  }), "   ", __jsx("path", {
    d: "M58.952,385.746c-11.05-0.038-20.029,8.91-20.029,19.961v85.832c0,11.023,8.937,19.961,19.961,19.961h0.001\t\t\t\tc11.024,0,19.961-8.937,19.961-19.961v-85.832C78.845,394.709,69.951,385.783,58.952,385.746z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 1549
    }
  }), "   ", __jsx("path", {
    d: "M451.118,171.165c-11.024,0-19.961,8.937-19.961,19.961V491.54c0,11.023,8.937,19.961,19.961,19.961h0.001\t\t\t\tc11.023,0,19.961-8.937,19.961-19.961V191.126C471.079,180.101,462.143,171.165,451.118,171.165z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 1762
    }
  }), "   ", __jsx("path", {
    d: "M293.426,276.958c-11.024,0-19.961,8.937-19.961,19.961v194.62c0,11.023,8.937,19.961,19.961,19.961h0.001\t\t\t\tc11.023,0,19.961-8.937,19.961-19.961v-194.62C313.387,285.895,304.451,276.958,293.426,276.958z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 1977
    }
  }), "   ", __jsx("path", {
    d: "M372.2,239.032c-10.996,0.04-19.888,8.966-19.888,19.961V491.54c0,11.023,8.937,19.961,19.961,19.961h0.001\t\t\t\tc11.023,0,19.961-8.937,19.961-19.961V258.993C392.233,247.94,383.252,238.991,372.2,239.032z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86,
      columnNumber: 2192
    }
  })), __jsx("h3", {
    className: "text-3xl font-normal text-blue-800",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88,
      columnNumber: 13
    }
  }, "Aumenta tus ventas"), __jsx("span", {
    className: "text-gray-600 font-light text-lg",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 91,
      columnNumber: 13
    }
  }, "Desm\xE1rcate de la competencia sobresaliendo con dise\xF1os, fotograf\xEDas y sitios web profesionales.")), __jsx("div", {
    className: "text-center mt-10 lg:mt-0 lg:mx-8 lg:flex-1",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 97,
      columnNumber: 11
    }
  }, __jsx("svg", {
    className: "fill-current text-limon w-auto h-12 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 98,
      columnNumber: 13
    }
  }, __jsx("path", {
    d: "M471.268,40.658c-2.12,30.113-10.922,87.499-46.458,123.035c-9.789,9.788-19.747,16.661-29.601,20.43\t\t\tc-10.313,3.944-15.477,15.502-11.533,25.815c3.046,7.964,10.631,12.858,18.679,12.858c2.374,0,4.788-0.426,7.138-1.325\t\t\tc15.138-5.789,29.804-15.716,43.592-29.504c58.674-58.674,59.083-156.572,58.808-171.001c0.004-0.085,0.007-0.17,0.01-0.255\t\t\tc0.362-11.004-8.241-20.23-19.245-20.637c-4.482-0.171-110.543-3.343-172.651,58.766c-23.903,23.903-35.935,49.916-35.76,77.315\t\t\tc0.143,22.582,8.769,40.626,14.372,51.363c-18.455,21.022-33.17,44.786-43.808,70.832c-2.282,5.587-4.354,11.232-6.221,16.927\t\t\tc-8.59-14.408-19.815-31.254-34.218-49.57c5.574-10.902,13.25-28.16,13.384-49.45c0.174-27.4-11.857-53.413-35.76-77.316\t\t\tC129.887,36.832,23.828,40.013,19.343,40.176C8.409,40.58-0.13,49.692,0.118,60.605c-0.285,12.647-0.546,112.119,58.799,171.464\t\t\tc23.729,23.729,49.699,35.761,77.188,35.761c0.351,0,28.044-0.165,51.443-11.323c7.184,9.437,13.245,18.142,18.268,25.845\t\t\tc21.231,32.561,29.149,54.842,29.286,55.234c0.368,1.067,0.82,2.079,1.342,3.039c-0.22,4.317-0.338,8.648-0.338,12.994v25.114\t\t\tc-8.963-4.231-18.812-6.484-28.938-6.484c-34.802,0-63.571,26.328-67.435,60.113c-30.097,3.183-53.625,28.718-53.625,59.646\t\t\tc0,11.042,8.951,19.993,19.993,19.993s19.993-8.951,19.993-19.993c0-11.024,8.969-19.993,19.993-19.993\t\t\tc2.717,0,5.344,0.531,7.808,1.578c7.229,3.072,15.589,1.626,21.368-3.695c5.778-5.323,7.903-13.536,5.432-20.993\t\t\tc-0.941-2.84-1.418-5.793-1.418-8.779c0-15.379,12.512-27.89,27.89-27.89c14.388,0,26.59,11.215,27.779,25.533\t\t\tc0.725,8.728,7.047,15.97,15.598,17.865c8.548,1.895,17.34-1.995,21.686-9.6c4.936-8.64,14.164-14.006,24.081-14.006\t\t\tc14.004,0,25.827,10.464,27.501,24.343c0.716,5.935,4.05,11.237,9.087,14.455c5.039,3.218,11.252,4.013,16.936,2.166\t\t\tc1.998-0.649,4.078-0.978,6.181-0.978c11.024,0,19.993,8.969,19.993,19.993c0,11.042,8.951,19.993,19.993,19.993\t\t\ts19.993-8.951,19.993-19.993c0-32.374-25.781-58.84-57.891-59.944c-10.651-23.773-34.531-40.022-61.793-40.022\t\t\tc-5.795,0-11.49,0.755-16.97,2.182c15.226-25.541,49.525-57.164,66.82-63.072c10.449-3.57,16.026-14.934,12.456-25.383\t\t\tc-3.57-10.448-14.934-16.025-25.383-12.456c-16.458,5.623-37.605,21.466-56.207,40.324c4.503-48.333,25.29-93.343,59.676-128.417\t\t\tl83.548-85.219c7.73-7.885,7.604-20.543-0.28-28.273c-7.885-7.73-20.544-7.604-28.273,0.28l-63.32,64.587\t\t\tc-2.312-6.133-4.097-13.12-4.145-20.692c-0.104-16.542,7.762-32.5,24.05-48.788C383.805,51.591,441.157,42.783,471.268,40.658z\t\t\t M185.288,191.787c-18.536-19.891-40.638-40.685-66.878-61.381c-8.669-6.839-21.241-5.354-28.079,3.316\t\t\ts-5.353,21.242,3.316,28.079c26.895,21.213,48.971,42.495,66.86,62.225c-8.882,2.537-18.608,3.817-24.402,3.817\t\t\tc-16.731,0-32.731-7.866-48.913-24.049c-35.542-35.542-44.34-92.935-46.459-123.033c30.125,2.126,87.47,10.937,122.988,46.455\t\t\tc16.287,16.287,24.155,32.246,24.05,48.786C187.734,181.623,186.741,186.92,185.288,191.787z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 99,
      columnNumber: 15
    }
  })), __jsx("h3", {
    className: "text-3xl font-normal text-blue-800",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 101,
      columnNumber: 13
    }
  }, "Expande tu marca"), __jsx("span", {
    className: "text-gray-600 font-light text-lg",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 104,
      columnNumber: 13
    }
  }, "Genera confianza en tus clientes potenciales mejorando tu identidad corporativa y llega a m\xE1s personas promocionando en redes sociales.")), __jsx("div", {
    className: "text-center mt-10 lg:mt-0 lg:mx-8 lg:flex-1",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 111,
      columnNumber: 11
    }
  }, __jsx("svg", {
    className: "fill-current text-limon w-auto h-12 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 112,
      columnNumber: 13
    }
  }, __jsx("path", {
    d: "M497.278,186.256c-7.424-8.156-20.053-8.751-28.21-1.327c-8.157,7.424-8.751,20.054-1.328,28.21\t\tc2.648,2.909,3.935,7.221,3.935,13.185c0,10.379-3.976,13.972-7.865,16.119c-4.443,2.451-9.818,3.126-11.612,3.185h-13.534\t\tc-8.045-37.764-27.95-69.206-58.728-92.247c-13.995-10.476-30.005-19.049-47.719-25.629\t\tc7.543-12.897,11.651-27.705,11.651-42.881C343.867,38.073,305.794,0,258.996,0c-46.798,0-84.872,38.073-84.872,84.872\t\tc0,4.99,0.441,9.943,1.303,14.805c-22.649-19.203-50.673-29.731-82.529-30.769c-7.2-0.226-13.959,3.421-17.704,9.569\t\tc-3.745,6.148-3.892,13.835-0.384,20.122c0.157,0.28,14.144,25.61,19.443,55.791c-20.16,18.697-29.679,39.774-36.868,55.69\t\tc-2.687,5.951-6.288,13.925-8.606,16.579H20.356c-11.029,0-19.97,8.94-19.97,19.97v79.879c0,11.029,8.94,19.97,19.97,19.97\t\th20.857c11.082,25.85,28.568,48.533,51.422,66.718l-6.622,16.061c-6.099,14.795-6.073,31.079,0.076,45.853\t\tc6.149,14.775,17.682,26.27,32.476,32.369c7.447,3.07,15.161,4.523,22.757,4.523c23.555-0.002,45.944-13.982,55.464-37.075\t\tl7.161-17.37c9.994,1.163,20.167,1.75,30.402,1.75h1.365c16.12,0,31.765-1.131,46.707-3.372l7.829,18.991\t\tc12.591,30.539,47.679,45.144,78.221,32.553c14.794-6.099,26.328-17.594,32.476-32.369c6.149-14.774,6.176-31.057,0.076-45.853\t\tl-14.501-35.172c-4.204-10.198-15.875-15.055-26.075-10.851c-10.196,4.204-15.054,15.877-10.851,26.074l14.501,35.172\t\tc2.033,4.931,2.024,10.359-0.025,15.285c-2.05,4.925-5.894,8.757-10.826,10.79c-10.181,4.195-21.876-0.671-26.074-10.851\t\tl-13.973-33.892c-3.675-8.916-13.197-13.93-22.627-11.92c-17.039,3.635-35.498,5.476-54.862,5.476h-1.365\t\tc-13.39,0-26.618-1.178-39.32-3.502c-9.269-1.692-18.464,3.32-22.056,12.033l-13.112,31.806\t\tc-4.198,10.178-15.894,15.046-26.074,10.85c-4.932-2.033-8.776-5.865-10.826-10.79c-2.05-4.926-2.059-10.353-0.025-15.285\t\tl12.605-30.573c3.638-8.824,0.523-18.988-7.433-24.259c-26.23-17.378-44.95-41.365-54.136-69.365\t\tc-2.687-8.2-10.341-13.744-18.971-13.744H40.326v-39.94h9.309c26.047,0,36.083-22.223,44.148-40.079\t\tc7.168-15.871,15.292-33.859,33.902-48.139c5.485-4.21,8.403-10.955,7.713-17.834c-1.641-16.362-5.687-31.94-10.1-45.009\t\tc15.931,6.324,29.458,17.055,40.42,32.11c4.477,6.15,12.058,9.223,19.553,7.922c14.538-2.519,31.05-3.797,49.076-3.797h1.365\t\tc39.191,0,167.063,9.648,167.063,133.798c0,11.168-1.074,21.857-3.194,31.771c-2.306,10.786,4.569,21.398,15.354,23.703\t\tc10.778,2.297,21.397-4.569,23.703-15.355c2.705-12.653,4.077-26.151,4.077-40.12h9.652c2.427,0,15.209-0.339,28.568-7.017\t\tc19.782-9.89,30.677-28.438,30.677-52.226C511.614,210.417,506.656,196.562,497.278,186.256z M290.846,116.566\t\tc-17.336-3.137-35.779-4.735-55.131-4.735h-1.365c-3.782,0-7.512,0.052-11.187,0.154c-5.884-7.771-9.099-17.307-9.099-27.113 c0-24.776,20.156-44.932,44.932-44.932s44.932,20.156,44.932,44.932C303.928,96.919,299.297,108.097,290.846,116.566z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 113,
      columnNumber: 15
    }
  })), __jsx("h3", {
    className: "text-3xl font-normal text-blue-800",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 115,
      columnNumber: 13
    }
  }, "Accesible"), __jsx("span", {
    className: "text-gray-600 font-light text-lg",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 118,
      columnNumber: 13
    }
  }, "Empieza con lo b\xE1sico y luego escala. A la medida de las micro y peque\xF1as empresas.")))), __jsx("section", {
    className: "py-20 lg:py-32 text-center text-2xl lg:text-4xl bg-crema",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 126,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 lg:max-w-screen-lg mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 127,
      columnNumber: 9
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 128,
      columnNumber: 11
    }
  }, __jsx("button", {
    className: "px-5 py-2 bg-aquamarina-oscuro hover:bg-teal-500 active:bg-green-300 text-white rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 129,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-8 inline-block pr-2 -mt-2",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 130,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 131,
      columnNumber: 17
    }
  })), "Contactar")))), __jsx("section", {
    id: "servicios",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 139,
      columnNumber: 7
    }
  }, __jsx("section", {
    id: "diseno-grafico",
    className: "py-20 lg:py-32 bg-orange-100",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 140,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 141,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 142,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_icon_design_qvdf-aqua-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 143,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 145,
      columnNumber: 13
    }
  }, __jsx("h3", {
    className: "pt-5 text-4xl lg:text-5xl font-thin leading-tight text-aquamarina-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 146,
      columnNumber: 15
    }
  }, "Dise\xF1o gr\xE1fico"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 149,
      columnNumber: 15
    }
  }, "La comunicaci\xF3n visual con un mensaje claro es indispensable para competir en el mercado digital y desmarcarte ante tu p\xFAblico."))), __jsx("div", {
    className: "mt-16 lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 155,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 156,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 157,
      columnNumber: 15
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 158,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 159,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "M511.433,142.874c-5.522-51.601-34.546-81.534-65.09-106.192c-42.93-34.659-97.487-36.598-99.788-36.664  c-7.236-0.188-13.985,3.495-17.707,9.682c-3.722,6.188-3.821,13.899-0.258,20.179c14.932,26.319,7.981,45.652-0.821,70.13  c-6.672,18.555-14.233,39.586-10.98,64.083c3.524,26.522,16.608,50.006,36.843,66.122c0.445,0.354,0.903,0.686,1.352,1.031v60.23  c-27.258,1.593-47.946,7.215-62.923,17.062c-18.365,12.076-28.073,30.562-28.073,53.459c0,16.314,4.907,29.635,9.237,41.387  c4.165,11.307,7.763,21.073,7.763,33.61c0,5.465-0.95,15.808-7.309,23.682c-6.066,7.509-16.39,11.316-30.69,11.316  c-115.724,0-202.991-92.855-202.991-215.991c0-119.098,96.893-215.991,215.991-215.991c4.572,0,9.199,0.145,13.753,0.43  c11.022,0.684,20.519-7.688,21.209-18.711c0.69-11.023-7.687-20.519-18.71-21.209c-5.383-0.337-10.851-0.508-16.252-0.508  c-68.377,0-132.661,26.628-181.011,74.977C26.628,123.339,0,187.623,0,256c0,68.946,24.124,133.028,67.93,180.441  c45.01,48.718,107.181,75.548,175.06,75.548c33.605,0,52.159-14.239,61.806-26.184c10.441-12.925,16.19-30.262,16.19-48.813  c0-19.669-5.435-34.421-10.228-47.435c-3.783-10.27-6.771-18.38-6.771-27.562c0-9.539,2.912-15.344,10.051-20.038  c6.211-4.084,18.18-8.808,40.947-10.402v118.005c-0.65,15.645,4.886,30.777,15.62,42.663  c10.732,11.88,25.448,18.873,41.438,19.687c1.043,0.053,2.082,0.079,3.118,0.079c14.858,0,28.998-5.428,40.105-15.459  c11.881-10.731,18.874-25.447,19.687-41.437c0.561-11.031-7.927-20.428-18.957-20.99c-11.039-0.56-20.428,7.928-20.99,18.957  c-0.27,5.32-2.597,10.217-6.55,13.787c-3.953,3.57-9.051,5.393-14.381,5.116c-5.32-0.271-10.217-2.597-13.786-6.55  c-2.145-2.374-5.688-7.399-5.333-14.38l0.026-0.508V249.309c6.154,1.204,12.453,1.835,18.836,1.835  c3.533,0,7.094-0.187,10.661-0.567c3.554-0.377,7.056-0.947,10.501-1.691v110.109c0,11.045,8.954,19.999,19.999,19.999  s19.999-8.954,19.999-19.999V229.963c5.579-4.444,10.711-9.531,15.299-15.214C506.702,194.406,514.215,168.882,511.433,142.874z   M459.157,189.623c-9.709,12.024-23.524,19.547-38.9,21.18c-15.245,1.617-30.055-2.599-41.702-11.877  c-12.105-9.641-19.959-23.883-22.114-40.102c-1.973-14.851,3.097-28.954,8.969-45.282c6.901-19.19,14.989-41.688,10.755-68.113  c14.008,3.949,30.635,10.736,45.053,22.376c28.352,22.89,46.68,44.144,50.444,79.327  C473.307,162.507,468.866,177.597,459.157,189.623z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 21
    }
  }), "  ", __jsx("path", {
    d: "M223.99,110.006c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999  c0-16.541-13.457-29.999-29.999-29.999C237.448,80.008,223.99,93.465,223.99,110.006z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 2324
    }
  }), "  ", __jsx("path", {
    d: "M169.218,179.226c11.701-11.702,11.701-30.743,0-42.444l-0.001-0.001c-11.703-11.701-30.744-11.701-42.446,0.001  c-11.701,11.702-11.701,30.743,0.001,42.445c5.851,5.851,13.536,8.776,21.222,8.776S163.366,185.077,169.218,179.226z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 2512
    }
  }), "  ", __jsx("path", {
    d: "M79.997,263c0,16.541,13.457,29.999,29.999,29.999c16.541,0,29.999-13.457,29.999-29.999s-13.457-29.999-29.999-29.999  C93.454,233.001,79.997,246.459,79.997,263z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 2750
    }
  }), "  ", __jsx("path", {
    d: "M150.994,391.994c8.013,0,15.545-3.121,21.212-8.789c5.666-5.667,8.786-13.202,8.786-21.218  c0-8.015-3.12-15.55-8.787-21.219c-11.698-11.698-30.728-11.696-42.423,0l-0.001,0.001c-5.666,5.667-8.786,13.202-8.786,21.218  c0,8.016,3.12,15.55,8.786,21.218C135.447,388.873,142.98,391.994,150.994,391.994z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 160,
      columnNumber: 2923
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 162,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 163,
      columnNumber: 21
    }
  }, "Dise\xF1o:"), " Creamos una identidad visual b\xE1sica para tu marca, logo, esquema de colores, fuentes y estilo gr\xE1fico.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 167,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 168,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m401.738281 442.710938c6.046875 9.242187 3.453125 21.636718-5.792969 27.683593-41.585937 27.195313-89.9375 41.582031-139.824218 41.605469-.039063 0-.082032 0-.121094 0-68.097656.003906-132.101562-26.234375-180.253906-73.890625-48.183594-47.683594-75.078125-111.46875-75.7343752-179.597656-.6601568-68.558594 25.4453122-133.210938 73.5039062-182.054688 48.066406-48.851562 112.265625-76 180.773437-76.45312475.5625 0 1.136719-.00390625 1.699219-.00390625 53.46875 0 104.816407 16.546875 148.613281 47.90625 43.277344 30.988281 75.519532 73.652344 93.238282 123.378906 3.707031 10.40625-1.722656 21.847656-12.125 25.554688-10.40625 3.707031-21.84375-1.722656-25.554688-12.125-30.898437-86.707032-112.84375-144.714844-204.195312-144.714844-.476563 0-.9375 0-1.414063.003906-57.796875.378906-111.964843 23.289063-152.523437 64.507813-40.546875 41.210937-62.570313 95.765625-62.015625 153.613281 1.132812 117.949219 98.019531 213.875 215.992187 213.875h.097656c42.097657-.019531 82.886719-12.148438 117.953126-35.082031 9.242187-6.046875 21.636718-3.453125 27.683593 5.792969zm110.261719-165.710938c0 50.179688-40.820312 91-91 91-32.0625 0-60.304688-16.667969-76.519531-41.796875-20.71875 26.058594-52.679688 42.796875-88.480469 42.796875-62.308594 0-113-50.691406-113-113s50.691406-113 113-113c28.277344 0 54.160156 10.441406 74 27.667969v-3.535157c0-11.042968 8.953125-20 20-20s20 8.957032 20 20v109.867188c0 28.121094 22.878906 51 51 51s51-22.878906 51-51c0-11.046875 8.953125-20 20-20s20 8.953125 20 20zm-183-21c0-40.25-32.746094-73-73-73-40.25 0-73 32.75-73 73 0 40.253906 32.75 73 73 73 40.253906 0 73-32.746094 73-73zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 169,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 171,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 172,
      columnNumber: 21
    }
  }, "Social Media:"), " Perfil, portada, gr\xE1ficas para publicaciones e historias.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 176,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 177,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m312 256c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20s8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20zm-20 57h-192c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h192c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm-80 80h-112c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h112c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm260-61v-56h-80v37h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20v39h20c11.046875 0 20 8.953125 20 20s-8.953125 20-20 20h-20c0 22.054688 17.945312 40 40 40 22.097656 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.847656 80-79.910156 80-.246094 0-.484375 0-.726563-.003906-.121093.003906-.242187.003906-.363281.003906h-351c-44.113281 0-80-35.886719-80-80v-412c0-11.046875 8.953125-20 20-20h352c11.046875 0 20 8.953125 20 20v216h100c11.046875 0 20 8.953125 20 20v76c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20zm-109.261719 140c-6.828125-11.773438-10.738281-25.4375-10.738281-40v-392h-312v392c0 22.054688 17.945312 40 40 40zm-282.738281-293v-80c0-11.046875 8.953125-20 20-20h191c11.046875 0 20 8.953125 20 20v80c0 11.046875-8.953125 20-20 20h-191c-11.046875 0-20-8.953125-20-20zm40-20h151v-40h-151zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 178,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 180,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 181,
      columnNumber: 21
    }
  }, "Papeler\xEDa:"), " Membretes, tarjetas personales, banners y m\xE1s.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 185,
      columnNumber: 17
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 186,
      columnNumber: 19
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-aquamarina-claro hover:bg-teal-400 active:bg-teal-400 text-blue-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 187,
      columnNumber: 21
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 188,
      columnNumber: 23
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 189,
      columnNumber: 25
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 197,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 198,
      columnNumber: 15
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_4__["default"], {
    slides: funcionalidades2Slides,
    slidesData: funcionalidades2SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 203,
          columnNumber: 28
        }
      }, funcionalidades2SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 199,
      columnNumber: 17
    }
  }))))), __jsx("section", {
    id: "fotografia",
    className: "py-20 lg:py-32",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 214,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 215,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 lg:pl-16 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 216,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_product_photography_91i2-limon.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 217,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 219,
      columnNumber: 13
    }
  }, __jsx("h3", {
    className: "pt-6 text-4xl lg:text-5xl font-thin leading-tight text-green-600",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 220,
      columnNumber: 15
    }
  }, "Fotograf\xEDa"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 223,
      columnNumber: 15
    }
  }, "Cr\xE9a un v\xEDnculo entre tu p\xFAblico y tu empresa despertando su inter\xE9s y confianza."))), __jsx("div", {
    className: "mt-16 lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 228,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 229,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 230,
      columnNumber: 15
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 231,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 232,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m432 462h-352c-44.113281 0-80-35.886719-80-80v-230c0-44.113281 35.886719-80 80-80h81.773438c4.277343 0 8.167968-2.527344 9.90625-6.433594l15.761718-35.402344c8.160156-18.324218 26.394532-30.164062 46.453125-30.164062h115.289063c19.722656 0 37.828125 11.558594 46.125 29.445312l16.828125 36.273438c1.769531 3.8125 5.632812 6.28125 9.839843 6.28125h10.023438c44.113281 0 80 35.886719 80 80v130c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20v-130c0-22.054688-17.945312-40-40-40h-10.023438c-19.722656 0-37.824218-11.558594-46.125-29.445312l-16.828124-36.273438c-1.769532-3.816406-5.632813-6.28125-9.839844-6.28125h-115.289063c-4.28125 0-8.167969 2.527344-9.910156 6.433594l-15.761719 35.402344c-8.160156 18.324218-26.394531 30.164062-46.449218 30.164062h-81.773438c-22.054688 0-40 17.945312-40 40v230c0 22.054688 17.945312 40 40 40h352c22.054688 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.886719 80-80 80zm-12-205c0-68.925781-56.074219-125-125-125s-125 56.074219-125 125 56.074219 125 125 125 125-56.074219 125-125zm-40 0c0 46.867188-38.132812 85-85 85s-85-38.132812-85-85 38.132812-85 85-85 85 38.132812 85 85zm-285 4c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20-20 8.953125-20 20 8.953125 20 20 20zm0-70c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20-20 8.953125-20 20 8.953125 20 20 20zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 233,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 235,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-green-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 236,
      columnNumber: 21
    }
  }, "Fotograf\xEDa de productos: "), "Resalt\xE1 las caracter\xEDsticas principales de tus productos.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 241,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 242,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m432 0h-352c-44.113281 0-80 35.886719-80 80v280c0 44.113281 35.886719 80 80 80h167c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20h-167c-10.230469 0-19.570312-3.863281-26.648438-10.203125l87.84375-107.632813 52.316407 54.621094c.007812.011719.015625.019532.023437.027344l.019532.023438c.335937.347656.6875.660156 1.042968.980468.140625.125.265625.265625.414063.390625 0 0 .003906.003907.003906.003907 1.558594 1.332031 3.273437 2.390624 5.082031 3.167968.296875.125.597656.222656.898438.335938.304687.113281.609375.238281.917968.339844.402344.128906.816407.222656 1.226563.328124.210937.050782.421875.121094.640625.167969.402344.085938.8125.140625 1.21875.203125.226562.03125.453125.082032.683594.109375.355468.042969.710937.050781 1.066406.074219.285156.019531.574219.050781.859375.054688.28125.007812.558594-.011719.839844-.019532.363281-.007812.730469-.007812 1.089843-.035156.203126-.011719.398438-.050781.597657-.070312.441406-.046876.882812-.09375 1.320312-.167969.132813-.023438.265625-.0625.398438-.089844.496093-.097656.992187-.199219 1.480469-.332031.109374-.03125.214843-.074219.320312-.105469.507812-.148437 1.015625-.308594 1.515625-.496094.164063-.066406.324219-.148437.492187-.21875.429688-.175781.859376-.351562 1.277344-.5625.566406-.28125 1.117188-.59375 1.65625-.929687.015625-.011719.03125-.019532.046875-.027344 1.125-.707031 2.191407-1.535156 3.179688-2.484375.003906-.003906.007812-.007813.011719-.007813.347656-.335937.660156-.6875.980468-1.042968.128906-.140625.265625-.265625.390625-.414063l.007813-.007812c.015625-.019531.03125-.035157.046875-.054688l154.753906-181.152343 79.789063 93.230468c3.800781 4.441406 9.351562 6.996094 15.195312 6.996094h19c11.046875 0 20-8.953125 20-20v-155c0-44.113281-35.886719-80-80-80zm40 203.074219-78.804688-92.078125c-3.800781-4.441406-9.351562-6.996094-15.195312-6.996094-.003906 0-.007812 0-.007812 0-5.847657.003906-11.402344 2.5625-15.199219 7.011719l-155.640625 182.1875-52.707032-55.035157c-3.964843-4.136718-9.488281-6.375-15.238281-6.148437-5.726562.226563-11.078125 2.898437-14.699219 7.339844l-84.507812 103.535156v-262.890625c0-22.054688 17.945312-40 40-40h352c22.054688 0 40 17.945312 40 40zm-332-131.074219c-33.085938 0-60 26.914062-60 60s26.914062 60 60 60 60-26.914062 60-60-26.914062-60-60-60zm0 80c-11.027344 0-20-8.972656-20-20s8.972656-20 20-20 20 8.972656 20 20-8.972656 20-20 20zm304.5 126c-19.105469 0-35.492188 6.308594-47.5 18.035156-12.007812-11.726562-28.394531-18.035156-47.5-18.035156-18.445312 0-35.429688 6.90625-47.816406 19.445312-12.695313 12.851563-19.683594 30.949219-19.683594 50.960938 0 42.292969 36.351562 69.398438 65.5625 91.175781 13.5625 10.113281 26.375 19.664063 33.859375 28.960938 3.796875 4.714843 9.523437 7.457031 15.578125 7.457031s11.78125-2.742188 15.578125-7.457031c7.484375-9.296875 20.296875-18.847657 33.859375-28.960938 29.210938-21.777343 65.5625-48.882812 65.5625-91.175781 0-20.011719-6.988281-38.109375-19.683594-50.960938-12.386718-12.539062-29.371094-19.445312-47.816406-19.445312zm-21.96875 129.511719c-8.6875 6.476562-17.542969 13.082031-25.53125 20.070312-7.988281-6.988281-16.84375-13.59375-25.53125-20.070312-24.320312-18.132813-49.46875-36.882813-49.46875-59.105469 0-18.46875 10.792969-30.40625 27.5-30.40625 14.152344 0 19.722656 5.980469 22.679688 10.476562 3.835937 5.832032 4.769531 12.238282 4.886718 13.175782.617188 10.464844 9.199219 18.4375 19.695313 18.570312h.253906c10.425781 0 19.109375-8.273437 19.925781-18.699218.003906-.054688.800782-6.335938 4.335938-12.1875 2.945312-4.867188 8.554687-11.335938 23.222656-11.335938 16.707031 0 27.5 11.9375 27.5 30.40625 0 22.222656-25.148438 40.972656-49.46875 59.105469zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 243,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 245,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-green-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 246,
      columnNumber: 21
    }
  }, "Fotograf\xEDa corporativa: "), "Present\xE1 tu local, el equipo de trabajo, los procesos de la empresa y las dem\xE1s \xE1reas de inter\xE9s.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 251,
      columnNumber: 17
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 252,
      columnNumber: 19
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-limon hover:bg-green-400 active:bg-green-400 text-green-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 253,
      columnNumber: 21
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 254,
      columnNumber: 23
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 255,
      columnNumber: 25
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 263,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 264,
      columnNumber: 15
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_4__["default"], {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269,
          columnNumber: 28
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 265,
      columnNumber: 17
    }
  }))))), __jsx("section", {
    id: "promocion-digital",
    className: "py-20 lg:py-32 bg-orange-100",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 280,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 281,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 282,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_email_capture_x8kv-aqua-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 283,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 285,
      columnNumber: 13
    }
  }, __jsx("h3", {
    className: "pt-5 text-4xl lg:text-5xl font-thin leading-tight text-aquamarina-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 286,
      columnNumber: 15
    }
  }, "Promoci\xF3n digital"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 289,
      columnNumber: 15
    }
  }, "Genera m\xE1s ventas mostrando tus anuncios utilizando promociones pagadas en redes sociales."))), __jsx("div", {
    className: "mt-16 lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 295,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 296,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 297,
      columnNumber: 15
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 298,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 299,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m375 220c-11.046 0-20-8.954-20-20 0-5.514-4.486-10-10-10s-10 4.486-10 10c0 11.046-8.954 20-20 20s-20-8.954-20-20c0-27.57 22.43-50 50-50s50 22.43 50 50c0 11.046-8.954 20-20 20zm-157-20c0-27.57-22.43-50-50-50s-50 22.43-50 50c0 11.046 8.954 20 20 20s20-8.954 20-20c0-5.514 4.486-10 10-10s10 4.486 10 10c0 11.046 8.954 20 20 20s20-8.954 20-20zm170.461 275.107c9.448-5.723 12.467-18.021 6.745-27.468-5.723-9.448-18.022-12.468-27.468-6.745-33.595 20.35-72.233 31.106-111.738 31.106-119.103 0-216-96.897-216-216s96.897-216 216-216 216 96.897 216 216c0 42.589-12.665 84.044-36.626 119.885-6.139 9.183-3.672 21.603 5.511 27.742 9.183 6.14 21.604 3.671 27.742-5.511 28.374-42.442 43.373-91.585 43.373-142.116 0-68.38-26.629-132.667-74.98-181.02-48.353-48.351-112.64-74.98-181.02-74.98s-132.667 26.629-181.02 74.98c-48.351 48.353-74.98 112.64-74.98 181.02s26.629 132.667 74.98 181.02c48.353 48.351 112.64 74.98 181.02 74.98 46.813 0 92.617-12.757 132.461-36.893zm-46.461-184.107c-6.512 0-12.28 3.127-15.932 7.945 0 0-26.018 28.055-70.068 28.055s-70.068-28.055-70.068-28.055c-3.652-4.818-9.42-7.945-15.932-7.945-11.046 0-20 8.954-20 20 0 5.691 2.388 10.814 6.204 14.456 3.617 4.012 39.099 41.544 99.796 41.544s96.179-37.532 99.796-41.544c3.816-3.642 6.204-8.765 6.204-14.456 0-11.046-8.954-20-20-20z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 300,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 302,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 303,
      columnNumber: 21
    }
  }, "P\xFAblico objetivo:"), " Identifica tu p\xFAblico objetivo y dirig\xED tus anuncios de manera efectiva.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 307,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 308,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m412 232.121094c11.046875 0 20 8.953125 20 20v24c0 11.046875-8.953125 20-20 20h-46c-11.046875 0-20-8.953125-20-20v-24c0-11.046875 8.953125-20 20-20zm60-36v-60h-432v159.878906c0 22.054688 17.945312 40 40 40h352c22.054688 0 40-17.945312 40-40 0-11.046875 8.953125-20 20-20s20 8.953125 20 20c0 44.113281-35.886719 80-80 80h-352c-44.113281 0-80-35.886719-80-80v-216c0-44.113281 35.886719-80 80-80h352c44.113281 0 80 35.886719 80 80v116.121094c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20zm0-100v-16.121094c0-22.054688-17.945312-40-40-40h-352c-22.054688 0-40 17.945312-40 40v16.121094zm-372 80c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h165c11.046875 0 20-8.953125 20-20s-8.953125-20-20-20zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 309,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 311,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 312,
      columnNumber: 21
    }
  }, "Gesti\xF3n de pagos:"), " Gestionamos por t\xED el pago a las redes sociales por las promociones.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 316,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 317,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m512 182v49c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20v-29.714844l-195.109375 195.105469c-7.808594 7.8125-20.472656 7.8125-28.28125 0l-66.148437-66.148437-69.625 71.691406c-3.921876 4.039062-9.132813 6.066406-14.347657 6.066406-5.023437 0-10.046875-1.878906-13.933593-5.652344-7.921876-7.695312-8.109376-20.355468-.414063-28.28125l83.761719-86.25c3.730468-3.839844 8.84375-6.027344 14.199218-6.066406h.148438c5.300781 0 10.390625 2.105469 14.140625 5.859375l66.359375 66.359375 181.964844-181.96875h-32.714844c-11.046875 0-20-8.953125-20-20s8.953125-20 20-20h50c27.570312 0 50 22.429688 50 50zm-20 109c-11.046875 0-20 8.953125-20 20v121c0 22.054688-17.945312 40-40 40h-352c-22.054688 0-40-17.945312-40-40v-260h116v41c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-113c0-33.085938 26.914062-60 60-60s60 26.914062 60 60v32h-59c-11.046875 0-20 8.953125-20 20s8.953125 20 20 20h59v41c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-113c0-55.140625-44.859375-100-100-100s-100 44.859375-100 100v32h-136c-11.046875 0-20 8.953125-20 20v280c0 44.113281 35.886719 80 80 80h352c44.113281 0 80-35.886719 80-80v-121c0-11.046875-8.953125-20-20-20zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 318,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 320,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 321,
      columnNumber: 21
    }
  }, "Mide el imp\xE1cto:"), " Conoce m\xE1s sobre el comportamiento de tu p\xFAblico con tu marca.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 325,
      columnNumber: 17
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 326,
      columnNumber: 19
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-aquamarina-claro hover:bg-teal-400 active:bg-teal-400 text-blue-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 327,
      columnNumber: 21
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 328,
      columnNumber: 23
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 329,
      columnNumber: 25
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 337,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 338,
      columnNumber: 15
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_4__["default"], {
    slides: funcionalidades2Slides,
    slidesData: funcionalidades2SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 343,
          columnNumber: 28
        }
      }, funcionalidades2SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 339,
      columnNumber: 17
    }
  }))))), __jsx("section", {
    id: "diseno-web",
    className: "py-20 lg:py-32",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 354,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 355,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 lg:pl-16 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 356,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_add_to_cart_vkjp-celeste-oscuro.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 357,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 359,
      columnNumber: 13
    }
  }, __jsx("h3", {
    className: "pt-6 text-4xl lg:text-5xl font-thin leading-tight text-celeste-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 360,
      columnNumber: 15
    }
  }, "Dise\xF1o web"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 363,
      columnNumber: 15
    }
  }, "Gu\xEDa a tus potenciales clientes en el proceso de venta mostrando lo que ofreces y qui\xE9n eres de manera eficiente. Ten el control de tu informaci\xF3n en internet."))), __jsx("div", {
    className: "mt-16 lg:flex lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 370,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 371,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "my-10 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 372,
      columnNumber: 15
    }
  }, __jsx("div", {
    className: "relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 373,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 374,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m452 0h-392c-33.085938 0-60 26.914062-60 60v332c0 44.113281 35.886719 80 80 80h352c44.113281 0 80-35.886719 80-80 0-11.046875-8.953125-20-20-20s-20 8.953125-20 20c0 22.054688-17.945312 40-40 40h-352c-22.054688 0-40-17.945312-40-40v-271h432v171c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-232c0-33.085938-26.914062-60-60-60zm-81 40c11.027344 0 20 8.972656 20 20s-8.972656 20-20 20-20-8.972656-20-20 8.972656-20 20-20zm100 20c0 11.027344-8.972656 20-20 20s-20-8.972656-20-20 8.972656-20 20-20 20 8.972656 20 20zm-431 0c0-11.027344 8.972656-20 20-20h254.441406c-2.222656 6.261719-3.441406 12.988281-3.441406 20 0 7.386719 1.347656 14.460938 3.800781 21h-274.800781zm216 138.042969c-12.007812-11.730469-28.394531-18.042969-47.5-18.042969-18.398438 0-35.347656 6.8125-47.726562 19.1875-12.75 12.742188-19.773438 30.707031-19.773438 50.582031 0 41.960938 36.40625 68.761719 65.65625 90.296875 13.5625 9.980469 26.371094 19.410156 33.84375 28.574219 3.800781 4.65625 9.488281 7.359375 15.5 7.359375s11.699219-2.703125 15.5-7.359375c7.472656-9.164063 20.28125-18.59375 33.84375-28.574219 29.25-21.535156 65.65625-48.335937 65.65625-90.296875 0-19.875-7.023438-37.839843-19.773438-50.582031-12.378906-12.375-29.328124-19.1875-47.726562-19.1875-19.105469 0-35.492188 6.3125-47.5 18.042969zm75 51.726562c0 21.738281-25.097656 40.214844-49.371094 58.082031-8.71875 6.417969-17.613281 12.964844-25.628906 19.902344-8.015625-6.9375-16.910156-13.484375-25.628906-19.902344-24.273438-17.867187-49.371094-36.34375-49.371094-58.082031 0-18.082031 10.792969-29.769531 27.5-29.769531 14.148438 0 19.71875 5.984375 22.675781 10.484375 3.839844 5.84375 4.773438 12.261719 4.890625 13.199219.621094 10.5625 9.339844 18.820312 19.933594 18.820312 10.597656 0 19.3125-8.261718 19.933594-18.820312.117187-.9375 1.050781-7.355469 4.890625-13.199219 2.957031-4.5 8.527343-10.484375 22.675781-10.484375 16.707031 0 27.5 11.6875 27.5 29.769531zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 375,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 377,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 378,
      columnNumber: 21
    }
  }, "Landing pages:"), " Capta m\xE1s interesados en tus productos y servicios con una p\xE1gina web optimizada para venta.")), __jsx("div", {
    className: "mt-6 relative",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 382,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-celeste-oscuro w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 383,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "M507.519,116.384C503.721,111.712,498.021,109,492,109H129.736l-1.484-13.632l-0.053-0.438C121.099,40.812,74.583,0,20,0 C8.954,0,0,8.954,0,20s8.954,20,20,20c34.506,0,63.923,25.749,68.512,59.928l23.773,218.401C91.495,327.765,77,348.722,77,373 c0,0.167,0.002,0.334,0.006,0.5C77.002,373.666,77,373.833,77,374c0,33.084,26.916,60,60,60h8.138 c-2.034,5.964-3.138,12.355-3.138,19c0,32.532,26.467,59,59,59s59-26.468,59-59c0-6.645-1.104-13.036-3.138-19h86.277 c-2.034,5.964-3.138,12.355-3.138,19c0,32.532,26.467,59,59,59c32.533,0,59-26.468,59-59c0-32.532-26.467-59-59-59H137 c-11.028,0-20-8.972-20-20c0-0.167-0.002-0.334-0.006-0.5c0.004-0.166,0.006-0.333,0.006-0.5c0-11.028,8.972-20,20-20h255.331 c35.503,0,68.084-21.966,83.006-55.962c4.439-10.114-0.161-21.912-10.275-26.352c-10.114-4.439-21.912,0.161-26.352,10.275 C430.299,300.125,411.661,313,392.331,313h-240.39L134.09,149h333.308l-9.786,46.916c-2.255,10.813,4.682,21.407,15.495,23.662 c1.377,0.288,2.75,0.426,4.104,0.426c9.272,0,17.59-6.484,19.558-15.92l14.809-71C512.808,127.19,511.317,121.056,507.519,116.384 z M399,434c10.477,0,19,8.523,19,19s-8.523,19-19,19s-19-8.523-19-19S388.523,434,399,434z M201,434c10.477,0,19,8.524,19,19 c0,10.477-8.523,19-19,19s-19-8.523-19-19S190.523,434,201,434z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 384,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 386,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-blue-700 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 387,
      columnNumber: 21
    }
  }, "eCommerce:"), " Muestra tu cat\xE1logo de productos y automatiza tu recepci\xF3n de pedidos con un sistema de venta online.")), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 391,
      columnNumber: 17
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 392,
      columnNumber: 19
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-celeste-oscuro hover:bg-teal-400 active:bg-teal-400 text-white text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 393,
      columnNumber: 21
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 394,
      columnNumber: 23
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 395,
      columnNumber: 25
    }
  })), "Solicitar servicios"))))), __jsx("div", {
    className: "p-3 lg:w-1/2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 403,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "bg-white border border-gray-300 shadow-lg rounded-lg w-64 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 404,
      columnNumber: 15
    }
  }, __jsx(_lib_react_instagram_zoom_slider__WEBPACK_IMPORTED_MODULE_4__["default"], {
    slides: funcionalidades1Slides,
    slidesData: funcionalidades1SlidesData,
    slideOverlayBottom: function slideOverlayBottom(currentSlide) {
      return __jsx("div", {
        className: "font-semibold my-4 mx-2 text-center",
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 409,
          columnNumber: 28
        }
      }, funcionalidades1SlidesData[currentSlide].title);
    },
    slideIndicatorTimeout: null,
    showButtons: true,
    activeDotColor: "#409cff",
    dotColor: "#ccc",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 405,
      columnNumber: 17
    }
  }))))), __jsx("section", {
    id: "informatica",
    className: "py-20 lg:py-32 bg-orange-100",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 420,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 421,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-0 text-left lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 422,
      columnNumber: 13
    }
  }, __jsx("img", {
    className: "mx-auto h-48 lg:h-64",
    src: "vectors/undraw_feeling_proud_qne1-limon.svg",
    alt: "Dise\xF1o gr\xE1fico",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 423,
      columnNumber: 15
    }
  })), __jsx("div", {
    className: "px-6 sm:px-20 text-left lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 425,
      columnNumber: 13
    }
  }, __jsx("h3", {
    className: "pt-5 text-4xl lg:text-5xl font-thin leading-tight text-green-600",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 426,
      columnNumber: 15
    }
  }, "Inform\xE1tica"), __jsx("p", {
    className: "pt-3 text-gray-600 text-xl lg:text-2xl font-hairline leading-relaxed",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 429,
      columnNumber: 15
    }
  }, "Cuenta con nosotros para cuidar tus equipos inform\xE1ticos, te ayudamos a elegir las computadoras e insumos que mejor se adapten a tu necesidad y te lo vendemos a un precio justo."))), __jsx("div", {
    className: "mt-16 lg:flex lg:flex-row-reverse lg:align-middle lg:max-w-screen-lg xl:max-w-screen-xl lg:mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 437,
      columnNumber: 11
    }
  }, __jsx("div", {
    className: "px-6 sm:px-20 text-left",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 438,
      columnNumber: 13
    }
  }, __jsx("div", {
    className: "my-10 lg:-mx-4 lg:mt-10 text-gray-600 font-light leading-relaxed text-lg lg:text-xl lg:flex",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 439,
      columnNumber: 15
    }
  }, __jsx("div", {
    className: "lg:px-4 relative lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 440,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 441,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m387.222656 106.800781c0-11.023437 8.9375-19.960937 19.964844-19.960937h44.914062c11.027344 0 19.964844 8.9375 19.964844 19.960937v117.78125c0 11.027344-8.9375 19.964844-19.964844 19.964844-11.023437 0-19.960937-8.9375-19.960937-19.964844v-97.816406h-24.953125c-11.027344 0-19.964844-8.9375-19.964844-19.964844zm116.820313 380.71875c-9.089844 15.328125-25.164063 24.480469-43.003907 24.480469h-410.144531c-18.15625 0-34.550781-9.363281-43.851562-25.046875-9.019531-15.210937-9.398438-33.445313-1.015625-48.777344l34.835937-63.714843v-267.660157c0-11.023437 8.9375-19.960937 19.964844-19.960937h36.929687c11.027344 0 19.964844 8.9375 19.964844 19.960937 0 11.027344-8.9375 19.964844-19.964844 19.964844h-16.96875v235.511719h351.347657v-36.882813c0-11.023437 8.9375-19.960937 19.964843-19.960937 11.027344 0 19.964844 8.9375 19.964844 19.960937v52.71875l32.835938 60.0625c8.535156 15.601563 8.210937 34.050781-.859375 49.34375zm-34.167969-30.191406-30.140625-55.125h-368.53125l-30.140625 55.128906c-2.15625 3.941407-.722656 7.492188.324219 9.257813.761719 1.285156 3.703125 5.484375 9.507812 5.484375h134.753907c.550781-10.535157 9.238281-18.914063 19.910156-18.914063h98.816406c10.671875 0 19.363281 8.378906 19.910156 18.914063h136.753906c5.1875 0 7.785157-3.441407 8.664063-4.917969 1.074219-1.816406 2.53125-5.511719.171875-9.828125zm-319.886719-387.277344 9.792969-18.503906c4.59375-8.671875 14.8125-12.71875 24.097656-9.550781l22.957032 7.839844c5.148437-4.027344 10.757812-7.398438 16.726562-10.042969l4.28125-23.421875c1.738281-9.484375 10-16.371094 19.636719-16.371094h17.472656c9.660156 0 17.9375 6.917969 19.648437 16.425781l4.246094 23.585938c.390625.175781.769532.378906 1.160156.5625.402344.191406.804688.386719 1.203126.582031.84375.417969 1.679687.84375 2.507812 1.289062.273438.148438.546875.300782.816406.453126.929688.511718 1.851563 1.035156 2.757813 1.585937.078125.046875.152343.097656.230469.144531 2.09375 1.277344 4.140624 2.632813 6.109374 4.101563l21.488282-8.425781c8.855468-3.472657 18.9375-.214844 24.082031 7.789062l10.894531 16.949219c5.625 8.746093 3.589844 20.347656-4.679687 26.660156l-19.816407 15.136719c.027344.375.039063.753906.0625 1.132812.023438.394532.046876.789063.066407 1.1875.054687 1.210938.089843 2.417969.089843 3.628906 0 1-.019531 2-.054687 3l19.210937 13.84375c7.699219 5.550782 10.429688 15.757813 6.519532 24.414063l-8.070313 17.863281c-4.257812 9.429688-15.015625 14.046875-24.777343 10.628906l-22.300782-7.792968c-5.496094 4.484375-11.539062 8.195312-18.007812 11.058594l-3.675782 22.96875c-1.550781 9.683593-9.90625 16.808593-19.710937 16.808593h-17.96875c-9.660156 0-17.933594-6.917969-19.648437-16.429687l-4.242188-23.582032c-.410156-.183593-.804688-.394531-1.210938-.585937-.371093-.179687-.742187-.359375-1.109374-.539063-.894532-.441406-1.777344-.890624-2.652344-1.363281-.195313-.105469-.386719-.214843-.582032-.320312-3.300781-1.816407-6.484374-3.839844-9.503906-6.113281l-21.699218 8.601562c-9.335938 3.699219-19.972657-.117188-24.828126-8.910156l-9.359374-16.949219c-4.714844-8.539063-2.566407-19.238281 5.078124-25.296875l19.125-15.15625c-.03125-.421875-.046874-.84375-.074218-1.269531-.023438-.40625-.050782-.8125-.066406-1.21875-.058594-1.21875-.09375-2.441407-.09375-3.660157 0-1.261718.03125-2.523437.089843-3.785156l-19.597656-12.96875c-8.5625-5.667968-11.425781-16.910156-6.621094-25.984375zm66.054688 42.738281c0 .765626.027343 1.53125.070312 2.292969.011719.214844.035157.421875.050781.636719.039063.554688.089844 1.109375.152344 1.664062.027344.238282.058594.476563.089844.714844.074219.53125.15625 1.0625.246094 1.589844.042968.238281.082031.476562.128906.710938.105469.546874.226562 1.089843.351562 1.628906 1.675782 7.039062 5.214844 13.355468 10.066407 18.402344.351562.367187.710937.726562 1.074219 1.078124.246093.238282.492187.480469.746093.710938.527344.484375 1.0625.949219 1.609375 1.402344.292969.238281.597656.46875.898438.703125.328125.253906.65625.507812.992187.75.335938.246093.671875.488281 1.011719.71875.386719.261719.777344.511719 1.167969.761719.527343.332031 1.0625.652343 1.601562.960937.335938.1875.667969.378906 1.011719.554687.371094.199219.742188.390626 1.121094.578126.414062.203124.828125.390624 1.25.578124.492187.222657.988281.433594 1.492187.636719.480469.191407.960938.378907 1.449219.550781.320312.113282.644531.222657.972656.328126.550782.183593 1.113282.351562 1.675782.507812 3.40625.949219 6.992187 1.464844 10.699218 1.464844 3.386719 0 6.671875-.425782 9.8125-1.222656.835938-.210938 1.660156-.457032 2.476563-.71875.289062-.09375.574219-.191407.855469-.292969.585937-.203125 1.164062-.421875 1.738281-.652344.480469-.195313.957031-.398437 1.425781-.609375.445312-.199219.886719-.402344 1.320312-.617188.410157-.203124.816407-.410156 1.21875-.625.371094-.199218.734376-.410156 1.097657-.621093.621093-.363281 1.234375-.738281 1.832031-1.128907.324219-.210937.640625-.425781.957031-.644531.367188-.253906.734375-.511719 1.09375-.777343.578125-.433594 1.144531-.878907 1.699219-1.34375.269531-.226563.53125-.460938.796875-.691407.503906-.445312 1.003906-.902343 1.488281-1.375.132813-.128906.269532-.25.398438-.382812.621094-.617188 1.214844-1.265625 1.796875-1.929688 4.464843-5.085937 7.660156-11.308593 9.085937-18.175781.085938-.40625.152344-.816406.226563-1.222656.042969-.25.09375-.492188.128906-.742188.097656-.605468.171875-1.21875.238281-1.832031.007813-.0625.019532-.121094.023438-.183594.144531-1.375.214844-2.757812.214844-4.136719 0-.765624-.027344-1.53125-.070313-2.296874-.011719-.210938-.035156-.421876-.046875-.632813-.042969-.5625-.09375-1.121094-.15625-1.679687-.027344-.234376-.058594-.464844-.089844-.699219-.074218-.542969-.15625-1.085938-.25-1.625-.042968-.226563-.078125-.449219-.121094-.671875-.113281-.582032-.242187-1.164063-.378906-1.742188-1.6875-7-5.222656-13.285156-10.0625-18.316406-.433594-.445312-.875-.882812-1.324218-1.308594-.167969-.160156-.332032-.324218-.5-.480468-.5625-.511719-1.140626-1.007813-1.726563-1.484376-.242187-.199218-.492187-.390624-.738281-.582031-.394532-.308593-.792969-.605469-1.195313-.894531-.277343-.199219-.554687-.402344-.835937-.597656-.585938-.398438-1.179688-.78125-1.78125-1.148438-.3125-.1875-.632813-.363281-.953125-.546875-.375-.214843-.753907-.425781-1.140625-.625-.339844-.179687-.683594-.359375-1.027344-.53125-.476562-.230469-.960938-.453125-1.449219-.667969-.433593-.191406-.871093-.375-1.3125-.550781-.480469-.191406-.960937-.375-1.449219-.550781-.316406-.113281-.636718-.21875-.957031-.324219-.550781-.179687-1.105469-.34375-1.667969-.5-3.40625-.949219-6.988281-1.46875-10.691406-1.46875-3.640625 0-7.164062.5-10.519531 1.417969-.550781.148438-1.101563.3125-1.648437.484375-.40625.132813-.808594.269531-1.207032.410156-.34375.121094-.683594.253907-1.019531.382813-.847656.328125-1.679687.683594-2.496094 1.066406-.160156.074219-.324219.144531-.484375.222656-.515625.25-1.023437.511719-1.527344.785156-.117187.0625-.230468.128907-.347656.191407-1.589844.878906-3.121094 1.863281-4.570312 2.945312-.0625.042969-.121094.09375-.183594.136719-.914062.691406-1.796875 1.417969-2.644531 2.183594-.082031.070312-.160157.140625-.238281.210937-.425782.382813-.835938.777344-1.242188 1.179688-.125.125-.246094.257812-.371094.382812-5.066406 5.167969-8.734375 11.703125-10.410156 18.996094-.15625.683594-.292969 1.371094-.414062 2.058594-.007813.042969-.015626.085937-.023438.128906-.121094.695313-.21875 1.394531-.300781 2.089844-.007813.058593-.015625.117187-.023438.175781-.074219.664062-.128906 1.324219-.171875 1.988281-.003906.09375-.015625.183594-.019531.277344-.042969.738281-.066406 1.476563-.066406 2.210937zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 442,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 444,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-green-600 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 445,
      columnNumber: 21
    }
  }, "Instalaci\xF3n de software: "), "Instalaci\xF3n y actualizaci\xF3n de sistemas operativos y programas utilitarios.")), __jsx("div", {
    className: "mt-6 lg:mt-0 lg:px-4 relative lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 450,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 451,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m266 512h-20c-33.085938 0-60-26.914062-60-60v-10.074219c0-2.648437-1.667969-5.050781-4.253906-6.117187l-.367188-.152344c-2.589844-1.074219-5.480468-.5625-7.351562 1.3125l-7.121094 7.125c-11.332031 11.332031-26.398438 17.570312-42.429688 17.570312-16.023437 0-31.09375-6.238281-42.425781-17.570312l-14.140625-14.144531c-11.332031-11.332031-17.574218-26.398438-17.574218-42.425781 0-16.027344 6.242187-31.09375 17.574218-42.425782l7.121094-7.125c1.875-1.871094 2.390625-4.757812 1.3125-7.351562l-.152344-.367188c-1.066406-2.585937-3.46875-4.253906-6.117187-4.253906h-10.074219c-33.085938 0-60-26.914062-60-60v-20c0-33.085938 26.914062-60 60-60h10.074219c2.648437 0 5.050781-1.667969 6.117187-4.253906l.152344-.363282c1.074219-2.59375.5625-5.480468-1.3125-7.355468l-7.125-7.121094c-11.332031-11.332031-17.570312-26.398438-17.570312-42.425781 0-16.027344 6.238281-31.097657 17.574218-42.429688l14.140625-14.140625c11.332031-11.332031 26.398438-17.574218 42.425781-17.574218 16.027344 0 31.097657 6.242187 42.429688 17.574218l7.121094 7.121094c1.871094 1.875 4.757812 2.390625 7.355468 1.3125l.367188-.152344c2.582031-1.066406 4.25-3.46875 4.25-6.117187v-10.074219c0-33.085938 26.917969-60 60-60h20c33.085938 0 60 26.914062 60 60v10.074219c0 2.648437 1.671875 5.050781 4.253906 6.117187l.367188.152344c2.59375 1.078125 5.480468.5625 7.355468-1.3125l7.121094-7.125c11.332032-11.332031 26.398438-17.570312 42.425782-17.570312 16.027343 0 31.09375 6.238281 42.425781 17.570312l14.144531 14.144531c11.332031 11.332031 17.570312 26.398438 17.570312 42.425781 0 16.027344-6.238281 31.09375-17.570312 42.425782l-7.125 7.125c-1.871094 1.871094-2.386719 4.761718-1.3125 7.355468l.152344.363282c1.066406 2.582031 3.46875 4.253906 6.117187 4.253906h10.074219c33.085938 0 60 26.914062 60 60v20c0 33.082031-26.914062 60-60 60h-10.074219c-2.648437 0-5.050781 1.667969-6.117187 4.25l-.152344.367188c-1.074219 2.59375-.5625 5.480468 1.3125 7.355468 7.8125 7.8125 7.8125 20.476563 0 28.285156-7.8125 7.8125-20.472656 7.808594-28.285156 0-13.363282-13.371093-17.277344-33.382812-9.964844-50.984374l.121094-.292969c7.269531-17.605469 24.183594-28.980469 43.085937-28.980469h10.074219c11.027344 0 20-8.972656 20-20v-20c0-11.027344-8.972656-20-20-20h-10.074219c-18.902343 0-35.816406-11.375-43.085937-28.980469l-.152344-.367187c-7.28125-17.53125-3.371094-37.542969 9.996094-50.910156l7.121094-7.121094c3.78125-3.777344 5.859374-8.800782 5.859374-14.140625 0-5.34375-2.078124-10.367188-5.855468-14.144531l-14.144532-14.144532c-3.777343-3.777344-8.800781-5.855468-14.140624-5.855468-5.34375 0-10.367188 2.078124-14.144532 5.855468l-7.121094 7.125c-13.371093 13.367188-33.378906 17.277344-50.984374 9.964844l-.292969-.121094c-17.605469-7.269531-28.980469-24.183594-28.980469-43.085937v-10.074219c0-11.027344-8.972656-20-20-20h-20c-11.027344 0-20 8.972656-20 20v10.074219c0 18.902343-11.375 35.816406-28.980469 43.085937l-.292969.121094c-17.605468 7.3125-37.617187 3.402344-50.984374-9.964844l-7.121094-7.121094c-3.777344-3.78125-8.800782-5.859374-14.144532-5.859374-5.339843 0-10.363281 2.078124-14.140624 5.859374l-14.140626 14.140626c-3.78125 3.777343-5.859374 8.800781-5.859374 14.144531 0 5.339843 2.078124 10.363281 5.859374 14.140625l7.121094 7.121094c13.367188 13.367187 17.277344 33.378906 9.964844 50.984374l-.121094.292969c-7.269531 17.605469-24.183594 28.980469-43.085937 28.980469h-10.074219c-11.027344 0-20 8.972656-20 20v20c0 11.027344 8.972656 20 20 20h10.074219c18.902343 0 35.816406 11.375 43.085937 28.980469l.121094.292969c7.3125 17.605468 3.402344 37.617187-9.964844 50.984374l-7.125 7.121094c-3.777344 3.777344-5.855468 8.800782-5.855468 14.140625 0 5.34375 2.078124 10.367188 5.855468 14.144531l14.144532 14.144532c3.777343 3.777344 8.800781 5.855468 14.140624 5.855468 5.34375 0 10.367188-2.078124 14.144532-5.855468l7.121094-7.125c13.371093-13.367188 33.378906-17.277344 50.984374-9.964844l.292969.121094c17.605469 7.269531 28.980469 24.183594 28.980469 43.085937v10.074219c0 11.027344 8.972656 20 20 20h20c11.027344 0 20-8.972656 20-20v-10.074219c0-18.902343 11.375-35.816406 28.980469-43.085937l.292969-.121094c17.605468-7.3125 37.617187-3.402344 50.984374 9.964844l7.121094 7.121094c3.777344 3.78125 8.800782 5.859374 14.144532 5.859374 5.339843 0 10.363281-2.078124 14.140624-5.859374 7.808594-7.808594 20.476563-7.808594 28.285157 0 7.808593 7.8125 7.808593 20.476562 0 28.285156-11.332031 11.332031-26.398438 17.574218-42.425781 17.574218-16.027344 0-31.097657-6.242187-42.429688-17.574218l-7.121094-7.121094c-1.871094-1.875-4.757812-2.390625-7.355468-1.3125l-.367188.152344c-2.582031 1.066406-4.25 3.46875-4.25 6.117187v10.074219c0 33.085938-26.914062 60-60 60zm-30.058594-166.113281c14.316406-.953125 27.679688-7.914063 36.667969-19.097657.253906-.316406.496094-.640624.730469-.972656l82.980468-117.203125c6.382813-9.015625 4.25-21.5-4.765624-27.882812-9.015626-6.382813-21.496094-4.246094-27.878907 4.769531l-82.546875 116.589844c-2.6875 3.09375-6.046875 3.765625-7.847656 3.886718-1.867188.121094-5.476562-.117187-8.632812-3.125-.101563-.09375-.203126-.1875-.304688-.28125l-52.859375-48.28125c-8.15625-7.449218-20.804687-6.878906-28.253906 1.277344s-6.875 20.808594 1.277343 28.257813l52.726563 48.15625c9.546875 9.007812 22.25 14.019531 35.3125 14.019531 1.132813 0 2.265625-.039062 3.394531-.113281zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 452,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 454,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-green-600 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 455,
      columnNumber: 21
    }
  }, "Mantenimiento de computadoras: "), "Limpieza general de los equipos y reparaciones.")), __jsx("div", {
    className: "mt-6 lg:mt-0 lg:px-4 relative lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 460,
      columnNumber: 17
    }
  }, __jsx("svg", {
    className: "absolute pt-3 fill-current text-limon w-auto h-10 mx-auto",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 461,
      columnNumber: 19
    }
  }, __jsx("path", {
    d: "m432 0h-231c-21.925781 0-43.101562 9.132812-58.15625 25.066406l-112.574219 112.335938c-18.425781 14.667968-30.269531 37.269531-30.269531 62.597656v232c0 44.113281 35.886719 80 80 80h231c22.71875 0 43.238281-9.535156 57.816406-24.792969.125-.121093.261719-.222656.386719-.347656l119.425781-118.347656.101563-.105469c15.003906-15.09375 23.269531-35.125 23.269531-56.40625v-232c0-44.113281-35.886719-80-80-80zm-87.554688 127.355469c-10.183593-4.707031-21.507812-7.355469-33.445312-7.355469h-62.964844l81.703125-80h102.15625zm-173.125-74.199219.472657-.488281c7.648437-8.167969 18.019531-12.667969 29.207031-12.667969h71.566406l-81.707031 80h-86.519531zm300.679688 258.84375c0 10.617188-4.113281 20.613281-11.585938 28.152344l-69.414062 68.792968v-36.945312c0-11.046875-8.953125-20-20-20s-20 8.953125-20 20v60c0 22.054688-17.945312 40-40 40h-231c-22.054688 0-40-17.945312-40-40v-232c0-22.054688 17.945312-40 40-40h98v92c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-92h93c22.054688 0 40 17.945312 40 40v72c0 11.046875 8.953125 20 20 20s20-8.953125 20-20v-72c0-17.632812-5.746094-33.941406-15.449219-47.179688l91.734375-91.636718c3.003906 5.613281 4.714844 12.015625 4.714844 18.816406zm-161 60v40c0 11.046875-8.953125 20-20 20s-20-8.953125-20-20v-40c0-11.046875 8.953125-20 20-20s20 8.953125 20 20zm0 0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 462,
      columnNumber: 21
    }
  })), __jsx("p", {
    className: "pl-16",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 464,
      columnNumber: 19
    }
  }, __jsx("span", {
    className: "text-green-600 font-normal",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 465,
      columnNumber: 21
    }
  }, "Venta de hardware: "), "Asesoramiento, venta e instalaci\xF3n de computadoras de escritorio, notebooks y accesorios."))), __jsx("div", {
    className: "mt-6 relative text-center pt-5",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 471,
      columnNumber: 15
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 472,
      columnNumber: 17
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-limon hover:bg-green-400 active:bg-green-400 text-green-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 473,
      columnNumber: 19
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 474,
      columnNumber: 21
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 475,
      columnNumber: 23
    }
  })), "Solicitar servicios"))))))), __jsx("section", {
    className: "py-20 px-6 lg:py-32 text-center text-2xl bg-celeste-oscuro",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 487,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "px-6 lg:max-w-screen-lg mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 488,
      columnNumber: 9
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 489,
      columnNumber: 11
    }
  }, __jsx("button", {
    className: "px-5 py-2 bg-limon hover:bg-green-400 active:bg-green-400 text-green-900 rounded-lg shadow lg:text-4xl",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 490,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-8 inline-block pr-2 -mt-2",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 491,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 492,
      columnNumber: 17
    }
  })), "Contactar")))), __jsx("div", {
    className: "lg:flex lg:items-center bg-gray-900",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 500,
      columnNumber: 7
    }
  }, __jsx("section", {
    id: "contactos",
    className: "py-20 px-6 bg-teal-900 text-white text-center lg:w-2/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 501,
      columnNumber: 9
    }
  }, __jsx("h2", {
    className: "mb-6 text-5xl font-thin text-white",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 502,
      columnNumber: 11
    }
  }, "Contactos"), __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 505,
      columnNumber: 11
    }
  }, __jsx("button", {
    className: "mb-6 inline-block px-5 py-2 bg-limon hover:bg-green-400 active:bg-green-400 text-green-900 text-xl rounded-lg shadow",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 506,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2 -mt-1",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 507,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 508,
      columnNumber: 17
    }
  })), "Contactar")), __jsx("div", {
    className: "text-lg text-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 513,
      columnNumber: 11
    }
  }, __jsx("a", {
    href: "https://bit.ly/contactar-tranquipy-landing-01",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 514,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2",
    viewBox: "0 -2 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 515,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m255.996094 507.949219h-.101563c-39.945312-.015625-79.550781-9.445313-115.042969-27.335938l-109.40625 24.886719c-6.675781 1.515625-13.667968-.488281-18.527343-5.3125s-6.910157-11.800781-5.441407-18.488281l23.707032-107.757813c-20.367188-37.613281-31.121094-78.808594-31.17968775-119.65625-.00390625-.140625-.00390625-.28125-.00390625-.417968.0273438-67.691407 26.796875-131.441407 75.371094-179.503907 48.464844-47.953125 112.609375-74.363281 180.621094-74.363281 68.164062.0273438 132.207031 26.460938 180.351562 74.425781 48.8125 48.632813 75.679688 112.433594 75.65625 179.644531-.019531 50.792969-15.25 99.882813-44.042969 141.957032-6.238281 9.117187-18.683593 11.449218-27.800781 5.210937-9.113281-6.238281-11.449219-18.683593-5.210938-27.800781 24.222657-35.398438 37.035157-76.679688 37.054688-119.382812.019531-56.496094-22.667969-110.226563-63.886719-151.292969-40.597656-40.449219-94.625-62.738281-152.128906-62.761719-118.917969 0-215.734375 95.742188-215.984375 213.507812 0 .128907.003906.253907.003906.378907-.015625 36.453125 10.238282 73.414062 29.648438 106.886719 2.511718 4.332031 3.308594 9.445312 2.230468 14.332031l-18.566406 84.394531 85.839844-19.527344c4.738281-1.074218 9.707031-.390625 13.980469 1.925782 31.363281 17.027343 66.898437 26.035156 102.765625 26.050781h.089844c39.003906 0 77.308593-10.546875 110.769531-30.496094 9.484375-5.660156 21.761719-2.554687 27.417969 6.933594s2.554687 21.761719-6.933594 27.421875c-39.652344 23.640625-85.039063 36.140625-131.253906 36.140625zm-56.484375-355.730469c-4.386719-9.71875-9.003907-10.050781-13.175781-10.21875h-11.222657c-3.90625 0-10.25 1.460938-15.613281 7.300781-5.367188 5.839844-20.496094 19.957031-20.496094 48.671875s20.984375 56.464844 23.910156 60.363282c2.929688 3.890624 40.511719 64.699218 100.027344 88.09375 49.464844 19.441406 59.53125 15.574218 70.265625 14.597656 10.738281-.96875 34.648438-14.113282 39.523438-27.742188 4.882812-13.625 4.882812-25.304687 3.417969-27.746094-1.460938-2.429687-5.367188-3.890624-11.222657-6.808593-5.859375-2.917969-34.558593-17.28125-39.925781-19.226563-5.367188-1.945312-9.269531-2.917968-13.175781 2.925782-3.90625 5.835937-15.40625 19.332031-18.824219 23.222656-3.414062 3.902344-6.832031 4.386718-12.6875 1.46875-5.855469-2.929688-24.523438-9.199219-46.894531-29.078125-17.410157-15.472657-29.488281-35.199219-32.90625-41.042969-3.417969-5.835938-.367188-9 2.570312-11.910156 2.628907-2.613282 6.1875-6.1875 9.117188-9.59375 2.921875-3.40625 3.757812-5.839844 5.707031-9.734375 1.953125-3.894531.976562-7.300781-.484375-10.222657-1.464844-2.917968-12.707031-31.78125-17.914063-43.320312",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 516,
      columnNumber: 17
    }
  })), "+595 991 273 333"), __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 520,
      columnNumber: 13
    }
  }), __jsx("a", {
    href: "mailto://hola.tranquipy@gmail.com",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 521,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 522,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "m512 256c0 11.046-8.954 20-20 20s-20-8.954-20-20c0-119.103-96.897-216-216-216s-216 96.897-216 216 96.897 216 216 216c34.845 0 68.098-8.035 98.835-23.883 9.819-5.063 21.88-1.207 26.941 8.611 5.062 9.817 1.206 21.88-8.611 26.941-35.946 18.534-76.462 28.331-117.165 28.331-68.38 0-132.667-26.629-181.02-74.98-48.351-48.353-74.98-112.64-74.98-181.02s26.629-132.667 74.98-181.02c48.353-48.351 112.64-74.98 181.02-74.98s132.667 26.629 181.02 74.98c48.351 48.353 74.98 112.64 74.98 181.02zm-24.138 72.782c7.962 7.656 8.21 20.316.555 28.279-16.083 16.727-37.718 25.939-60.917 25.939-32.312 0-60.433-18.238-74.644-44.953-23.314 27.479-58.08 44.953-96.856 44.953-70.028 0-127-56.972-127-127s56.972-127 127-127 127 56.972 127 127v42.5c0 24.537 19.963 44.5 44.5 44.5 12.218 0 23.611-4.853 32.083-13.663 7.654-7.962 20.316-8.211 28.279-.555zm-144.862-72.782c0-47.972-39.028-87-87-87s-87 39.028-87 87 39.028 87 87 87 87-39.028 87-87z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 523,
      columnNumber: 17
    }
  })), "hola.tranquipy@gmail.com"), __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 527,
      columnNumber: 13
    }
  }), __jsx("a", {
    href: "https://www.facebook.com/tranqui.pymes",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 528,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 529,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M492,352c11.046,0,20-8.954,20-20V80c0-44.112-35.888-80-80-80H80C35.888,0,0,35.888,0,80v352c0,44.112,35.888,80,80,80\t\t\th352c44.112,0,80-35.888,80-80c0-11.046-8.954-20-20-20c-11.046,0-20,8.954-20,20c0,22.056-17.944,40-40,40H327V298h45\t\t\tc11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20h-45v-55c0-22.056,17.944-40,40-40h10c11.046,0,20-8.954,20-20s-8.954-20-20-20\t\t\th-10c-44.112,0-80,35.888-80,80v55h-45c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h45v174H80c-22.056,0-40-17.944-40-40V80\t\t\tc0-22.056,17.944-40,40-40h352c22.056,0,40,17.944,40,40v252C472,343.046,480.954,352,492,352z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 530,
      columnNumber: 17
    }
  })), "tranqui.pymes"), __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 534,
      columnNumber: 13
    }
  }), __jsx("a", {
    href: "https://www.instagram.com/tranqui.py/",
    target: "blank",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 535,
      columnNumber: 13
    }
  }, __jsx("svg", {
    className: "fill-current h-5 inline-block pr-2",
    viewBox: "0 0 512 512",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 536,
      columnNumber: 15
    }
  }, __jsx("path", {
    d: "M363.273,0H148.728C66.719,0,0,66.719,0,148.728v214.544C0,445.281,66.719,512,148.728,512h214.544\t\t\tc23.662,0,46.291-5.399,67.26-16.049c19.98-10.146,37.717-24.968,51.293-42.864c6.675-8.799,4.954-21.345-3.846-28.021\t\t\tc-8.797-6.675-21.344-4.956-28.021,3.846C429.183,456.295,397.588,472,363.273,472H148.728C88.775,472,40,423.225,40,363.273\t\t\tV148.728C40,88.775,88.775,40,148.728,40h214.544C423.225,40,472,88.775,472,148.728V343c0,11.046,8.954,20,20,20\t\t\tc11.046,0,20-8.954,20-20V148.728C512,66.719,445.281,0,363.273,0z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 537,
      columnNumber: 17
    }
  }), "  ", __jsx("path", {
    d: "M256,118c-76.094,0-138,61.906-138,138s61.906,138,138,138s138-61.906,138-138S332.094,118,256,118z M256,354\t\t\tc-54.037,0-98-43.963-98-98s43.963-98,98-98s98,43.963,98,98S310.037,354,256,354z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 537,
      columnNumber: 546
    }
  }), "  ", __jsx("circle", {
    cx: "396",
    cy: "116",
    r: "20",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 537,
      columnNumber: 748
    }
  })), "tranqui.py"))), __jsx("section", {
    className: "py-10 px-12 text-white text-center lg:w-1/3",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 544,
      columnNumber: 9
    }
  }, "Hecho con", __jsx("svg", {
    viewBox: "0 2 18 17",
    className: "fill-current text-red-700 h-6 beating inline px-2",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 546,
      columnNumber: 11
    }
  }, __jsx("path", {
    d: "M8.946,4.925A4.708,4.708,0,0,1,13.263,2c2.6,0,4.473,2.224,4.709,4.876a4.852,4.852,0,0,1-.153,1.843,8.148,8.148,0,0,1-2.483,4.141l-6.389,5.7-6.281-5.7A8.147,8.147,0,0,1,.181,8.718,4.852,4.852,0,0,1,.029,6.876C.264,4.224,2.137,2,4.737,2A4.562,4.562,0,0,1,8.946,4.925Z",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 547,
      columnNumber: 13
    }
  })), "en Paraguay", __jsx("br", {
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 550,
      columnNumber: 11
    }
  }), "por las manos de", __jsx("div", {
    className: "-mb-2 text-center",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 552,
      columnNumber: 11
    }
  }, __jsx("img", {
    src: "red-tools-isologo.svg",
    alt: "Red Tools",
    className: "h-24 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 553,
      columnNumber: 13
    }
  })))));
}

/***/ })

})
//# sourceMappingURL=index.js.82ceedd29ef809cff336.hot-update.js.map