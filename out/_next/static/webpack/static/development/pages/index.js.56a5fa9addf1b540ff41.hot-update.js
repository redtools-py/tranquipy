webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./components/navbarHome.js":
/*!**********************************!*\
  !*** ./components/navbarHome.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return NavbarHome; });
/* harmony import */ var _babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/slicedToArray */ "./node_modules/@babel/runtime/helpers/esm/slicedToArray.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-scroll */ "./node_modules/react-scroll/modules/index.js");
/* harmony import */ var react_scroll__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_scroll__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "C:\\Users\\RedTools\\projects\\clients\\tranquipy\\components\\navbarHome.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement;


function NavbarHome(_ref) {
  var isSticky = _ref.isSticky;

  var _React$useState = react__WEBPACK_IMPORTED_MODULE_2___default.a.useState(false),
      _React$useState2 = Object(_babel_runtime_helpers_esm_slicedToArray__WEBPACK_IMPORTED_MODULE_0__["default"])(_React$useState, 2),
      isOpen = _React$useState2[0],
      setIsOpen = _React$useState2[1];

  return __jsx(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, __jsx("header", {
    className: "jsx-3022821742" + " " + "bg-teal-800 z-30 sm:flex sm:justify-between sm:items-center sm:px-4 sm:py-2 shadow-lg w-screen fixed top-0 left-0 navbar-ticky",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 7
    }
  }, __jsx("div", {
    className: "jsx-3022821742" + " " + "flex items-center justify-between px-4 py-2 sm:p-0",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 9
    }
  }, __jsx("div", {
    className: "jsx-3022821742",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 11
    }
  }, __jsx("img", {
    src: "icono-blanco.svg",
    alt: "Tranqui | Dise\xF1o e Inform\xE1tica",
    className: "jsx-3022821742" + " " + "w-auto h-8 mx-auto",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 13
    }
  })), __jsx("div", {
    className: "jsx-3022821742" + " " + "sm:hidden",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 11
    }
  }, __jsx("button", {
    onClick: function onClick() {
      return setIsOpen(!isOpen);
    },
    type: "button",
    className: "jsx-3022821742" + " " + "block text-gray-500 focus:text-white focus:outline-none hover:text-white",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, __jsx("svg", {
    x: "0px",
    y: "0px",
    viewBox: "0 0 512 512",
    className: "jsx-3022821742" + " " + "h-6 w-6 fill-current",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 15
    }
  }, isOpen && __jsx("path", {
    d: "M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717\t\t\tL34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859\t\t\tc-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287\t\t\tl221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285\t\t\tL284.286,256.002z",
    className: "jsx-3022821742",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 28
    }
  }), !isOpen && __jsx(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, __jsx("path", {
    d: "M438,0H74C33.196,0,0,33.196,0,74s33.196,74,74,74h364c40.804,0,74-33.196,74-74S478.804,0,438,0z M438,108H74\t\t\tc-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34S456.748,108,438,108z",
    className: "jsx-3022821742",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 31
    }
  }), "    ", __jsx("path", {
    d: "M438,182H74c-40.804,0-74,33.196-74,74s33.196,74,74,74h364c40.804,0,74-33.196,74-74S478.804,182,438,182z M438,290H74\t\t\tc-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34S456.748,290,438,290z",
    className: "jsx-3022821742",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 249
    }
  }), "    ", __jsx("path", {
    d: "M438,364H74c-40.804,0-74,33.196-74,74s33.196,74,74,74h264c11.046,0,20-8.954,20-20c0-11.046-8.954-20-20-20H74\t\t\tc-18.748,0-34-15.252-34-34s15.252-34,34-34h364c18.748,0,34,15.252,34,34s-15.252,34-34,34c-11.046,0-20,8.954-20,20\t\t\tc0,11.046,8.954,20,20,20c40.804,0,74-33.196,74-74S478.804,364,438,364z",
    className: "jsx-3022821742",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 476
    }
  })))))), __jsx("div", {
    className: "jsx-3022821742" + " " + ("px-2 pt-2 pb-4 " + (isOpen ? 'block' : 'hidden') + " sm:block sm:flex sm:py-0" || false),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }
  }, __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "header",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    href: "",
    className: "menu-btn block px-2 py-1 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 11
    }
  }, "Inicio"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "beneficios",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 11
    }
  }, "Beneficios"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "servicios",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 11
    }
  }, "Servicios"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "diseno-grafico",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 13
    }
  }, "Dise\xF1o gr\xE1fico"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "fotografia",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, "Fotograf\xEDa"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "promocion-digital",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 13
    }
  }, "Promoci\xF3n digital"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "diseno-web",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 13
    }
  }, "Dise\xF1o web"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "informatica",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn sm:hidden pl-5 mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 13
    }
  }, "Inform\xE1tica"), __jsx(react_scroll__WEBPACK_IMPORTED_MODULE_3__["Link"], {
    activeClass: "active",
    to: "contactos",
    spy: true,
    smooth: true,
    offset: 0,
    duration: 800,
    className: "menu-btn mt-1 block px-2 py-1 sm:ml-2 sm:mt-0 font-regular cursor-pointer",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49,
      columnNumber: 11
    }
  }, "Contactos"))), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_1___default.a, {
    id: "3022821742",
    __self: this
  }, ".navbar-ticky.jsx-3022821742{-webkit-animation:navbarMoveDown-jsx-3022821742 0.5s ease-in-out;animation:navbarMoveDown-jsx-3022821742 0.5s ease-in-out;}.navbar-absolute.jsx-3022821742{-webkit-animation:navbarMoveUp-jsx-3022821742 0.5s ease-in-out;animation:navbarMoveUp-jsx-3022821742 0.5s ease-in-out;}@-webkit-keyframes navbarMoveDown-jsx-3022821742{from{-webkit-transform:translateY(-5rem);-ms-transform:translateY(-5rem);transform:translateY(-5rem);}to{-webkit-transform:translateY(0rem);-ms-transform:translateY(0rem);transform:translateY(0rem);}}@keyframes navbarMoveDown-jsx-3022821742{from{-webkit-transform:translateY(-5rem);-ms-transform:translateY(-5rem);transform:translateY(-5rem);}to{-webkit-transform:translateY(0rem);-ms-transform:translateY(0rem);transform:translateY(0rem);}}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIkM6XFxVc2Vyc1xcUmVkVG9vbHNcXHByb2plY3RzXFxjbGllbnRzXFx0cmFucXVpcHlcXGNvbXBvbmVudHNcXG5hdmJhckhvbWUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBcURrQixBQUlRLEFBSUEsQUFJaUMsQUFHRCw2RkFDN0IsR0FIQSxzQkFMRixJQUpBIiwiZmlsZSI6IkM6XFxVc2Vyc1xcUmVkVG9vbHNcXHByb2plY3RzXFxjbGllbnRzXFx0cmFucXVpcHlcXGNvbXBvbmVudHNcXG5hdmJhckhvbWUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXHJcbmltcG9ydCB7IExpbmsgfSBmcm9tICdyZWFjdC1zY3JvbGwnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBOYXZiYXJIb21lKHsgaXNTdGlja3kgfSkge1xyXG4gIGNvbnN0IFtpc09wZW4sIHNldElzT3Blbl0gPSBSZWFjdC51c2VTdGF0ZShmYWxzZSlcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDw+XHJcbiAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPXtgYmctdGVhbC04MDAgei0zMCBzbTpmbGV4IHNtOmp1c3RpZnktYmV0d2VlbiBzbTppdGVtcy1jZW50ZXIgc206cHgtNCBzbTpweS0yIHNoYWRvdy1sZyB3LXNjcmVlbiBmaXhlZCB0b3AtMCBsZWZ0LTAgbmF2YmFyLXRpY2t5YH0+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGl0ZW1zLWNlbnRlciBqdXN0aWZ5LWJldHdlZW4gcHgtNCBweS0yIHNtOnAtMFwiPlxyXG4gICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgPGltZyBjbGFzc05hbWU9XCJ3LWF1dG8gaC04IG14LWF1dG9cIiBzcmM9XCJpY29uby1ibGFuY28uc3ZnXCIgYWx0PVwiVHJhbnF1aSB8IERpc2XDsW8gZSBJbmZvcm3DoXRpY2FcIi8+XHJcbiAgICAgICAgICAgIHsvKiA8c3ZnIGNsYXNzTmFtZT1cImZpbGwtY3VycmVudCB0ZXh0LXdoaXRlIHctYXV0byBoLTYgbXgtYXV0b1wiIHZpZXdCb3g9XCIwIDAgMTQ3LjE4IDUyLjkxXCI+PC9zdmc+ICovfVxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInNtOmhpZGRlblwiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIG9uQ2xpY2s9eygpID0+IHNldElzT3BlbighaXNPcGVuKX0gY2xhc3NOYW1lPVwiYmxvY2sgdGV4dC1ncmF5LTUwMCBmb2N1czp0ZXh0LXdoaXRlIGZvY3VzOm91dGxpbmUtbm9uZSBob3Zlcjp0ZXh0LXdoaXRlXCIgdHlwZT1cImJ1dHRvblwiPlxyXG4gICAgICAgICAgICAgIDxzdmcgY2xhc3NOYW1lPVwiaC02IHctNiBmaWxsLWN1cnJlbnRcIiB4PVwiMHB4XCIgeT1cIjBweFwiIHZpZXdCb3g9XCIwIDAgNTEyIDUxMlwiPlxyXG4gICAgICAgICAgICAgICAge2lzT3BlbiAmJiA8cGF0aCBkPVwiTTI4NC4yODYsMjU2LjAwMkw1MDYuMTQzLDM0LjE0NGM3LjgxMS03LjgxMSw3LjgxMS0yMC40NzUsMC0yOC4yODVjLTcuODExLTcuODEtMjAuNDc1LTcuODExLTI4LjI4NSwwTDI1NiwyMjcuNzE3XHRcdFx0TDM0LjE0Myw1Ljg1OWMtNy44MTEtNy44MTEtMjAuNDc1LTcuODExLTI4LjI4NSwwYy03LjgxLDcuODExLTcuODExLDIwLjQ3NSwwLDI4LjI4NWwyMjEuODU3LDIyMS44NTdMNS44NTgsNDc3Ljg1OVx0XHRcdGMtNy44MTEsNy44MTEtNy44MTEsMjAuNDc1LDAsMjguMjg1YzMuOTA1LDMuOTA1LDkuMDI0LDUuODU3LDE0LjE0Myw1Ljg1N2M1LjExOSwwLDEwLjIzNy0xLjk1MiwxNC4xNDMtNS44NTdMMjU2LDI4NC4yODdcdFx0XHRsMjIxLjg1NywyMjEuODU3YzMuOTA1LDMuOTA1LDkuMDI0LDUuODU3LDE0LjE0Myw1Ljg1N3MxMC4yMzctMS45NTIsMTQuMTQzLTUuODU3YzcuODExLTcuODExLDcuODExLTIwLjQ3NSwwLTI4LjI4NVx0XHRcdEwyODQuMjg2LDI1Ni4wMDJ6XCIgLz59XHJcbiAgICAgICAgICAgICAgICB7IWlzT3BlbiAmJiA8PjxwYXRoIGQ9XCJNNDM4LDBINzRDMzMuMTk2LDAsMCwzMy4xOTYsMCw3NHMzMy4xOTYsNzQsNzQsNzRoMzY0YzQwLjgwNCwwLDc0LTMzLjE5Niw3NC03NFM0NzguODA0LDAsNDM4LDB6IE00MzgsMTA4SDc0XHRcdFx0Yy0xOC43NDgsMC0zNC0xNS4yNTItMzQtMzRzMTUuMjUyLTM0LDM0LTM0aDM2NGMxOC43NDgsMCwzNCwxNS4yNTIsMzQsMzRTNDU2Ljc0OCwxMDgsNDM4LDEwOHpcIiAvPlx0XHRcdFx0PHBhdGggZD1cIk00MzgsMTgySDc0Yy00MC44MDQsMC03NCwzMy4xOTYtNzQsNzRzMzMuMTk2LDc0LDc0LDc0aDM2NGM0MC44MDQsMCw3NC0zMy4xOTYsNzQtNzRTNDc4LjgwNCwxODIsNDM4LDE4MnogTTQzOCwyOTBINzRcdFx0XHRjLTE4Ljc0OCwwLTM0LTE1LjI1Mi0zNC0zNHMxNS4yNTItMzQsMzQtMzRoMzY0YzE4Ljc0OCwwLDM0LDE1LjI1MiwzNCwzNFM0NTYuNzQ4LDI5MCw0MzgsMjkwelwiIC8+XHRcdFx0XHQ8cGF0aCBkPVwiTTQzOCwzNjRINzRjLTQwLjgwNCwwLTc0LDMzLjE5Ni03NCw3NHMzMy4xOTYsNzQsNzQsNzRoMjY0YzExLjA0NiwwLDIwLTguOTU0LDIwLTIwYzAtMTEuMDQ2LTguOTU0LTIwLTIwLTIwSDc0XHRcdFx0Yy0xOC43NDgsMC0zNC0xNS4yNTItMzQtMzRzMTUuMjUyLTM0LDM0LTM0aDM2NGMxOC43NDgsMCwzNCwxNS4yNTIsMzQsMzRzLTE1LjI1MiwzNC0zNCwzNGMtMTEuMDQ2LDAtMjAsOC45NTQtMjAsMjBcdFx0XHRjMCwxMS4wNDYsOC45NTQsMjAsMjAsMjBjNDAuODA0LDAsNzQtMzMuMTk2LDc0LTc0UzQ3OC44MDQsMzY0LDQzOCwzNjR6XCIgLz48Lz59XHJcbiAgICAgICAgICAgICAgPC9zdmc+XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9e2BweC0yIHB0LTIgcGItNCBgICsgKGlzT3BlbiA/ICdibG9jaycgOiAnaGlkZGVuJykgKyBgIHNtOmJsb2NrIHNtOmZsZXggc206cHktMGB9PlxyXG4gICAgICAgICAgPExpbmsgYWN0aXZlQ2xhc3M9XCJhY3RpdmVcIiB0bz1cImhlYWRlclwiIHNweT17dHJ1ZX0gc21vb3RoPXt0cnVlfSBvZmZzZXQ9ezB9IGR1cmF0aW9uPXs4MDB9IGhyZWY9XCJcIiBjbGFzc05hbWU9XCJtZW51LWJ0biBibG9jayBweC0yIHB5LTEgZm9udC1yZWd1bGFyIGN1cnNvci1wb2ludGVyXCI+XHJcbiAgICAgICAgICAgIEluaWNpb1xyXG4gICAgICAgICAgPC9MaW5rPlxyXG4gICAgICAgICAgPExpbmsgYWN0aXZlQ2xhc3M9XCJhY3RpdmVcIiB0bz1cImJlbmVmaWNpb3NcIiBzcHk9e3RydWV9IHNtb290aD17dHJ1ZX0gb2Zmc2V0PXswfSBkdXJhdGlvbj17ODAwfSBjbGFzc05hbWU9XCJtZW51LWJ0biBtdC0xIGJsb2NrIHB4LTIgcHktMSBzbTptbC0yIHNtOm10LTAgZm9udC1yZWd1bGFyIGN1cnNvci1wb2ludGVyXCI+XHJcbiAgICAgICAgICAgIEJlbmVmaWNpb3NcclxuICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgIDxMaW5rIGFjdGl2ZUNsYXNzPVwiYWN0aXZlXCIgdG89XCJzZXJ2aWNpb3NcIiBzcHk9e3RydWV9IHNtb290aD17dHJ1ZX0gb2Zmc2V0PXswfSBkdXJhdGlvbj17ODAwfSBjbGFzc05hbWU9XCJtZW51LWJ0biBtdC0xIGJsb2NrIHB4LTIgcHktMSBzbTptbC0yIHNtOm10LTAgZm9udC1yZWd1bGFyIGN1cnNvci1wb2ludGVyXCI+XHJcbiAgICAgICAgICAgIFNlcnZpY2lvc1xyXG4gICAgICAgICAgPC9MaW5rPlxyXG4gICAgICAgICAgICA8TGluayBhY3RpdmVDbGFzcz1cImFjdGl2ZVwiIHRvPVwiZGlzZW5vLWdyYWZpY29cIiBzcHk9e3RydWV9IHNtb290aD17dHJ1ZX0gb2Zmc2V0PXswfSBkdXJhdGlvbj17ODAwfSBjbGFzc05hbWU9XCJtZW51LWJ0biBzbTpoaWRkZW4gcGwtNSBtdC0xIGJsb2NrIHB4LTIgcHktMSBzbTptbC0yIHNtOm10LTAgZm9udC1yZWd1bGFyIGN1cnNvci1wb2ludGVyXCI+XHJcbiAgICAgICAgICAgICAgRGlzZcOxbyBncsOhZmljb1xyXG4gICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgIDxMaW5rIGFjdGl2ZUNsYXNzPVwiYWN0aXZlXCIgdG89XCJmb3RvZ3JhZmlhXCIgc3B5PXt0cnVlfSBzbW9vdGg9e3RydWV9IG9mZnNldD17MH0gZHVyYXRpb249ezgwMH0gY2xhc3NOYW1lPVwibWVudS1idG4gc206aGlkZGVuIHBsLTUgbXQtMSBibG9jayBweC0yIHB5LTEgc206bWwtMiBzbTptdC0wIGZvbnQtcmVndWxhciBjdXJzb3ItcG9pbnRlclwiPlxyXG4gICAgICAgICAgICAgIEZvdG9ncmFmw61hXHJcbiAgICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgICAgPExpbmsgYWN0aXZlQ2xhc3M9XCJhY3RpdmVcIiB0bz1cInByb21vY2lvbi1kaWdpdGFsXCIgc3B5PXt0cnVlfSBzbW9vdGg9e3RydWV9IG9mZnNldD17MH0gZHVyYXRpb249ezgwMH0gY2xhc3NOYW1lPVwibWVudS1idG4gc206aGlkZGVuIHBsLTUgbXQtMSBibG9jayBweC0yIHB5LTEgc206bWwtMiBzbTptdC0wIGZvbnQtcmVndWxhciBjdXJzb3ItcG9pbnRlclwiPlxyXG4gICAgICAgICAgICAgIFByb21vY2nDs24gZGlnaXRhbFxyXG4gICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgIDxMaW5rIGFjdGl2ZUNsYXNzPVwiYWN0aXZlXCIgdG89XCJkaXNlbm8td2ViXCIgc3B5PXt0cnVlfSBzbW9vdGg9e3RydWV9IG9mZnNldD17MH0gZHVyYXRpb249ezgwMH0gY2xhc3NOYW1lPVwibWVudS1idG4gc206aGlkZGVuIHBsLTUgbXQtMSBibG9jayBweC0yIHB5LTEgc206bWwtMiBzbTptdC0wIGZvbnQtcmVndWxhciBjdXJzb3ItcG9pbnRlclwiPlxyXG4gICAgICAgICAgICAgIERpc2XDsW8gd2ViXHJcbiAgICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICAgICAgPExpbmsgYWN0aXZlQ2xhc3M9XCJhY3RpdmVcIiB0bz1cImluZm9ybWF0aWNhXCIgc3B5PXt0cnVlfSBzbW9vdGg9e3RydWV9IG9mZnNldD17MH0gZHVyYXRpb249ezgwMH0gY2xhc3NOYW1lPVwibWVudS1idG4gc206aGlkZGVuIHBsLTUgbXQtMSBibG9jayBweC0yIHB5LTEgc206bWwtMiBzbTptdC0wIGZvbnQtcmVndWxhciBjdXJzb3ItcG9pbnRlclwiPlxyXG4gICAgICAgICAgICAgIEluZm9ybcOhdGljYVxyXG4gICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICA8TGluayBhY3RpdmVDbGFzcz1cImFjdGl2ZVwiIHRvPVwiY29udGFjdG9zXCIgc3B5PXt0cnVlfSBzbW9vdGg9e3RydWV9IG9mZnNldD17MH0gZHVyYXRpb249ezgwMH0gY2xhc3NOYW1lPVwibWVudS1idG4gbXQtMSBibG9jayBweC0yIHB5LTEgc206bWwtMiBzbTptdC0wIGZvbnQtcmVndWxhciBjdXJzb3ItcG9pbnRlclwiPlxyXG4gICAgICAgICAgICBDb250YWN0b3NcclxuICAgICAgICAgIDwvTGluaz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9oZWFkZXI+XHJcbiAgICAgIDxzdHlsZSBqc3g+e2BcclxuICAgICAgICAubmF2YmFyLXRpY2t5IHtcclxuICAgICAgICAgIGFuaW1hdGlvbjogbmF2YmFyTW92ZURvd24gMC41cyBlYXNlLWluLW91dFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLm5hdmJhci1hYnNvbHV0ZSB7XHJcbiAgICAgICAgICBhbmltYXRpb246IG5hdmJhck1vdmVVcCAwLjVzIGVhc2UtaW4tb3V0XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBAa2V5ZnJhbWVzIG5hdmJhck1vdmVEb3duIHtcclxuICAgICAgICAgIGZyb20ge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTVyZW0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdG8ge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMHJlbSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICBgfTwvc3R5bGU+XHJcbiAgICA8Lz5cclxuICApXHJcbn0iXX0= */\n/*@ sourceURL=C:\\\\Users\\\\RedTools\\\\projects\\\\clients\\\\tranquipy\\\\components\\\\navbarHome.js */"));
}

/***/ })

})
//# sourceMappingURL=index.js.56a5fa9addf1b540ff41.hot-update.js.map